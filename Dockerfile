FROM rust:1.75 as builder
WORKDIR /usr/src
COPY . .
RUN cargo build --release

FROM ubuntu:latest

RUN apt update
RUN apt install libc6

COPY --from=builder /usr/src/setup /usr/local/bin
COPY --from=builder /usr/src/target/release/key_holder /usr/local/bin/
COPY --from=builder /usr/src/target/release/metamask_client /usr/local/bin/
COPY --from=builder /usr/src/target/release/sequencer /usr/local/bin/

RUN ls /usr/local/bin/
EXPOSE 9949
EXPOSE 64001

CMD ["sequencer"]
