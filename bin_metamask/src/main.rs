use std::str::FromStr;
use std::sync::Arc;

use clap::Parser;
use futures::stream::FuturesUnordered;
use jsonrpsee::core::client::ClientT;
use jsonrpsee::rpc_params;
use jsonrpsee::ws_client::WsClientBuilder;
use jsonrpsee_core::client::Client;
use rand::random;
use serde::Deserialize;
use std::path::PathBuf;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;
use storage::TxStats;
use thiserror::Error;
use tokio::time::sleep;
use tokio::time::Instant;
use tokio::time::{Duration, Sleep};
use tokio_stream::StreamExt;
use tracing::info;

mod storage;

#[derive(Parser, Debug)]
struct Args {
    /// Server address to which transactions will be sent to
    #[arg(index = 1, default_value = "127.0.0.1:9949")]
    server_address: String,
    /// Stages of the benchmark, a sequence of ',' separated values in the format <duration_in_secs>:<tps>
    #[arg(long, short, value_delimiter = ',')]
    stages: Option<Vec<BenchmarkStage>>,
    #[arg(long, short)]
    output: Option<PathBuf>,
}

async fn send_tx(
    client: Arc<Client>,
    random_bytes: Vec<u8>,
    count: u64,
    benchmark_stage: u32,
) -> Result<TxStats, Box<dyn std::error::Error>> {
    //TODO: Use Instant instead. Could not find a nice way to get a u128 out of a instant easily
    let start = SystemTime::now();
    let start_i = Instant::now();
    let _: String = match client
        .request("submit_transaction", rpc_params![random_bytes])
        .await
    {
        Ok(res) => res,
        Err(e) => panic!("don't panic! {:?}", e),
    };
    let elapsed = start_i.elapsed().as_millis();
    let end = SystemTime::now();
    let start = start.duration_since(UNIX_EPOCH).map(|t| t.as_millis())?;
    let end = end.duration_since(UNIX_EPOCH).map(|t| t.as_millis())?;
    let tx_record = storage::TxStats {
        id: count,
        benchmark_stage,
        start,
        end,
        elapsed,
    };
    info!("transaction {} received in {:?} ms", count, elapsed);
    Ok(tx_record)
}

#[derive(Error, Debug)]
enum BenchmarkStageError {
    #[error("Benchmark stage is not in the format <duration_in_secs>:<tps> - input: {0}")]
    ParseError(String),
}

#[derive(Debug, Clone, Deserialize)]
struct BenchmarkStage {
    duration: Duration,
    tps: u64,
}

impl FromStr for BenchmarkStage {
    type Err = BenchmarkStageError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(':').collect();
        if parts.len() != 2 {
            return Err(BenchmarkStageError::ParseError(s.to_string()));
        };
        let secs_str: &str = *parts.first().unwrap();
        let tps_str: &str = *parts.get(1).unwrap();
        let secs: u64 =
            u64::from_str(secs_str).map_err(|_e| BenchmarkStageError::ParseError(s.to_string()))?;
        let duration: Duration = Duration::from_secs(secs);
        let tps: u64 =
            u64::from_str(tps_str).map_err(|_e| BenchmarkStageError::ParseError(s.to_string()))?;

        Ok(BenchmarkStage::new(duration, tps))
    }
}

impl BenchmarkStage {
    pub fn new(duration: Duration, tps: u64) -> Self {
        BenchmarkStage { duration, tps }
    }

    fn time_between_transactions(&self) -> Duration {
        Duration::from_secs(1).div_f32(self.tps as f32)
    }

    fn transactions(&self) -> u64 {
        self.duration.as_secs() * self.tps
    }

    async fn run(
        self,
        client: Arc<Client>,
        max_tx_size: usize,
        start_count: u64,
        storage: &Option<storage::TxLatencyStorage>,
        stage_number: u32,
    ) -> (u64, Vec<TxStats>) {
        let time_between_transactions = self.time_between_transactions();
        let mut requested: u64 = 0;
        let mut completed: u64 = 0;
        let mut tx_stats: Vec<TxStats> = Vec::new();
        let mut requests = FuturesUnordered::new();
        let transactions: u64 = self.transactions();

        let timer: Sleep = sleep(time_between_transactions);
        tokio::pin!(timer);

        loop {
            tokio::select! {
                () = &mut timer => {
                    timer.as_mut().reset(Instant::now() + time_between_transactions);

                    let random_bytes: Vec<u8> = (0..max_tx_size).map(|_| random::<u8>()).collect();
                    let next_tx_id: u64 = start_count +requested;
                    let client: Arc<Client> = client.clone();
                    requested += 1;
                    requests.push(async move {
                        send_tx(client, random_bytes, next_tx_id as u64, stage_number).await
                    })
                }

                Some(tx_record) = requests.next(), if !requests.is_empty() => {
                    match tx_record {
                        Err(e) => { tracing::error!("Could not get stats for transaction: {}", e)}
                        Ok(tx_record) => {
                            tx_stats.push(tx_record);
                        }
                    }
                    completed += 1;
                    if completed == transactions {
                        for tx_record in tx_stats.iter() {
                            match storage.as_ref() {
                                None => {},
                                Some(s) => {
                                    let write_result =
                                    s.save(tx_record).await;
                                    write_result.unwrap_or_else(|e| tracing::error!("Could not save stats for tx with id {}: {}", tx_record.id, e));
                                }
                            };
                        }
                        return (start_count + completed, tx_stats) };
                }
            }
        }
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();

    let args = Args::parse();

    let storage = match args.output.map(storage::TxLatencyStorage::from_path) {
        Some(Err(e)) => {
            tracing::warn!("Failed to open the CSV file: {e}");
            None
        }
        Some(Ok(res)) => Some(res),
        None => None,
    };

    let addr = args.server_address;
    let stages = args.stages.unwrap_or(vec![
        BenchmarkStage::new(Duration::from_secs(60), 10),
        BenchmarkStage::new(Duration::from_secs(60), 20),
        BenchmarkStage::new(Duration::from_secs(60), 30),
        BenchmarkStage::new(Duration::from_secs(60), 40),
        BenchmarkStage::new(Duration::from_secs(60), 50),
        BenchmarkStage::new(Duration::from_secs(60), 40),
        BenchmarkStage::new(Duration::from_secs(60), 30),
        BenchmarkStage::new(Duration::from_secs(60), 20),
        BenchmarkStage::new(Duration::from_secs(60), 10),
    ]);

    info!("addr: {}", addr);
    let url: String = format!("ws://{}", addr);

    let client: Arc<Client> = Arc::new(WsClientBuilder::default().build(url).await?);

    let tx_size: usize = 256;
    let start: Instant = Instant::now();

    let mut txs: u64 = 0;
    let mut stage_number: u32 = 0;
    let mut benchmark_stats: Vec<Vec<TxStats>> = Vec::new();
    for stage in stages.into_iter() {
        info!("Beginning benchmark stage {}. Transactions will be sent for {} seconds at a rate of {} tps", stage_number, stage.duration.as_secs(), stage.tps);
        let time_before_stage = Instant::now();
        let (new_txs, stage_stats) = stage
            .run(client.clone(), tx_size, txs, &storage, stage_number)
            .await;
        txs = new_txs;
        info!(
            "Stage {} completed in {:?}",
            stage_number,
            time_before_stage.elapsed()
        );
        benchmark_stats.push(stage_stats);
        stage_number += 1;
    }

    let duration: Duration = start.elapsed();
    println!("time it took for sending {txs} txs {:?}", duration);
    let mut count = 0;
    for stage_stats in benchmark_stats.iter_mut() {
        info!("Printing stats for stage {}", count);
        info!("{:?}", get_stats(stage_stats));
        count += 1;
    }
    let mut benchmark_stats = benchmark_stats.concat();
    info!("Printing stats for the whole benchmark (all stages)");
    info!("{:?}", get_stats(&mut benchmark_stats));

    Ok(())
}

#[derive(Debug)]
struct BenchmarkStats {
    transactions: usize,
    min: Option<u128>,
    max: Option<u128>,
    mean: f64,
    median: Option<u128>,
    p95: Option<u128>,
    std_dev: f64,
}

fn get_stats(stage_stats: &mut Vec<TxStats>) -> BenchmarkStats {
    stage_stats.sort_by_key(|tx_record| tx_record.elapsed);
    let transactions = stage_stats.len();
    let min = stage_stats.get(0).map(|tx_record| tx_record.elapsed);
    let max = stage_stats
        .get(transactions - 1)
        .map(|tx_record| tx_record.elapsed);
    let mean = stage_stats
        .iter()
        .map(|tx_record| tx_record.elapsed as f64)
        .sum::<f64>()
        / (transactions as f64);
    let median = if transactions == 0 {
        None
    } else if transactions % 2 == 1 {
        let mid_point = transactions / 2;
        stage_stats
            .get(mid_point)
            .map(|tx_record| tx_record.elapsed)
    } else {
        let lower_mid_point = (transactions / 2) - 1;
        let higher_mid_point = transactions / 2;
        // in this case transactions > 2, so both higher_mid_point and lower_mid_point are valid positions in stage_stats
        let lower = stage_stats.get(lower_mid_point).unwrap().elapsed;
        let higher = stage_stats.get(higher_mid_point).unwrap().elapsed;
        Some(higher + (higher - lower) / 2)
    };
    // not accurate, but good approximation since we send transactions in multiples of 100. Here `transactions / 20`
    // represents 5% of the transactions.
    let p95_index = transactions - (transactions / 20);
    let p95 = stage_stats
        .get(p95_index)
        .map(|tx_record| tx_record.elapsed);

    let variance_numerator: f64 = stage_stats
        .iter()
        .map(|x| (x.elapsed as f64 - mean).powf(2.0))
        .sum::<f64>();

    let std_dev = (variance_numerator / (transactions as f64)).sqrt();

    BenchmarkStats {
        transactions,
        min,
        max,
        mean,
        median,
        p95,
        std_dev,
    }
}
