use std::{fs::File, path::Path, sync::Arc};

use csv::{Writer, WriterBuilder};
use serde::Serialize;
use thiserror::Error;
use tokio::sync::RwLock;

#[derive(Serialize, Clone)]
pub struct TxStats {
    pub id: u64,
    pub benchmark_stage: u32,
    pub start: u128,
    pub end: u128,
    pub elapsed: u128,
}

#[derive(Error, Debug)]
pub enum BackendStorageError {
    #[error("Cannot open csv file in write mode {0}")]
    CsvError(csv::Error),
}

impl From<csv::Error> for BackendStorageError {
    fn from(e: csv::Error) -> Self {
        BackendStorageError::CsvError(e)
    }
}

#[derive(Clone)]
pub struct TxLatencyStorage {
    csv: Arc<RwLock<Writer<File>>>,
}

impl TxLatencyStorage {
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, BackendStorageError> {
        let writer = WriterBuilder::new().has_headers(true).from_path(path)?;

        Ok(TxLatencyStorage {
            csv: Arc::new(RwLock::new(writer)),
        })
    }

    pub async fn save(&self, record: &TxStats) -> Result<(), BackendStorageError> {
        let mut csv = self.csv.write().await;
        csv.serialize(record)?;
        Ok(())
    }
}
