# Threshold Encryption for Sequencer

This repository contains a proof of concept for enabling threshold encryption over a sequencer.
It is possible to test the development locally via `Docker` and `docker-compose`.

## Requirements:
- Docker (tested with `Docker version 24.0.2, build cb74dfc`)
- docker-compose (tested with `Docker Compose version v2.19.1`)
- A working rust release. Compilation has been tested with `Rust 1.76`.

## Compile and run the binaries

Assuming you have a working rust installation, run
```
cargo build --relase
```

The following binaries will be built:
- `sequencer`:
  - receives transactions from external entities,
  - sequences transactions in preblocks,
  - pushes produced preblocks to subscribed clients,
- `observer`:
  - receives transactions from external clients,
  - forwards them to the sequencer,
  - subscribeds to preblocks produced by the sequencer,
- `key_holder`:
  - produces distributed key shares for threshold encryption for each preblock level,
  - receives transactions from external clients, **encrypts them**  using the key-share, and forwards them to the sequencer.
  - subscribes to preblocks from the sequencer,
  - upon receiving a preblock, reveals the distributed key shares to other keyholders
  - notifies external clients that submitted transactions have been sequenced
- `metamask_client`:
  - generates random transactions in stages, each having a finite duration in seconds and a configurable tps rate,
  - measures end-to-end latency times for sequencing transactions

Note: the `observer` and `key_holder` nodes are bundled into a single executable, as we assume they will run in the same machine or local network.

Each command can be run using `cargo run --bin <binary_name> -- <args>`. For each binary, you can run
`cargo run --bin <binary_name> -- --help` to check what parameters can be configured.

## Docker-compose setup
This repo provides a `docker-compose` setup that can be used for local development and testing.
On-change recompilation of binaries with `cargo-watch` and `docker volume bind-mounts` is not implemented (yet),
meaning that if code changes need to be tested, the `docker-compose` setup will need to be shut down and
images will need to be built again.

To spin up a dev-environment with docker compose, simply run
```
docker-compose up -- build
```

This will setup a dev-environment composed of a sequencer and six key_holders.
The nodes are interconnected via a dedicated docker network interface with
CIDR `172.50.0.0/16`. One of key_holders forwards the port `9949`, and can be
used to connect a local metamask client outside the dev-environment to the sequencer network.
The metamask client tries to connect to a key_holder at the address `127.0.0.1:9949` by default.
To run a benchmark locally when the dev environment is up and running, simply run
```
cargo run --bin metamask_client
```
The latter command is equivalent to

```
cargo run --bin metamask_client -- 127.0.0.1:9949
```
