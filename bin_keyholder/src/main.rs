use std::error::Error as StdError;
use std::fs;
use std::net::{IpAddr, SocketAddr};
use std::option::Option;
use std::process::Command;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Arc;
use std::time::Duration;

use ark_bls12_381::Bls12_381;
use ark_ec::bls12::{Bls12Config, G2Prepared};
use ark_ff::Fp12;
use ark_serialize::CanonicalDeserialize;
use async_channel::{Receiver, Sender};
use clap::{arg, Parser};
use futures::{prelude::*, FutureExt};
use hyper::server::conn::AddrStream;
use jsonrpsee::core::async_trait;
use jsonrpsee::proc_macros::rpc;
use jsonrpsee::server::http::response;
use jsonrpsee::server::{
    stop_channel, ws, ConnectionGuard, ConnectionState, RpcServiceBuilder, ServerConfig, StopHandle,
};
use jsonrpsee::types::ErrorObjectOwned;
use jsonrpsee::ws_client::WsClientBuilder;
use jsonrpsee::Methods;
use jsonrpsee_core::client::async_client::Client;
use jsonrpsee_core::client::{ClientT, Error};
use jsonrpsee_core::params::ArrayParams;
use jsonrpsee_core::{rpc_params, ClientError};
use serde::Serialize;
use tokio::join;
use tokio::sync::{Mutex, MutexGuard};
use tokio::time::{sleep, Instant};
use tracing::info;

use group_threshold_cryptography::{
    decrypt_with_shared_secret, encrypt, Ciphertext, DecryptionShare,
    PrivateDecryptionContextSerde, Setup,
};

const ENCRYPTION_TIME: Duration = Duration::new(0, 500_000);
const SHARE_GEN_TIME: Duration = Duration::new(0, 500_000);
const SHARE_VERIFY_TIME: Duration = Duration::new(0, 500_000);
const COMBINE_TIME: Duration = Duration::new(0, 1_000_000);

#[derive(Parser, Clone)]
#[clap(disable_help_flag = true)]
struct Args {
    /// The rpc_address this keyholder will listen to
    #[arg(long, short, default_value = "0.0.0.0")]
    host: IpAddr,
    /// The port this keyholder will listen to
    #[arg(long, short, default_value = "64001")]
    port: u16,
    /// The addresses used by the key_holder. The first address is the sequencer, the other addresses are the key-holders.
    #[arg(index = 1, value_delimiter = ',')]
    ips: Vec<IpAddr>,
    // The validator index for the Threshold Encryption scheme
    #[arg(long, short)]
    index: u16,
}

type Transaction = Vec<u8>;

// Key shares for all encrypted txs in a preblock
// first element is the block level, second one is a list of key shares
type KeyShare = (usize, Vec<DecryptionShare<Bls12_381>>);

#[derive(Clone, Serialize)]
pub enum TransactionStatus {
    Failed,
    Passed,
}

// encrypted transactions
type Transactions = Vec<Ciphertext<Bls12_381>>;

type PreBlock = (usize, Transactions);

#[derive(Clone)]
struct PreBlocks {
    elements: Arc<Mutex<Vec<PreBlock>>>,
    sender: Sender<PreBlock>,
    receiver: Receiver<PreBlock>,
}

impl PreBlocks {
    fn new() -> Self {
        let (sender, receiver) = async_channel::unbounded();
        PreBlocks {
            elements: Arc::new(Mutex::new(vec![])),
            sender,
            receiver,
        }
    }

    async fn push(&mut self, level: usize, txs: Transactions) {
        let pb: (usize, Vec<Ciphertext<Bls12_381>>) = (level, txs.clone());
        self.elements.lock().await.push(pb.clone());
        let sender: Sender<PreBlock> = self.sender.clone();
        // Send the item to the sender stream
        sender.send(pb).await.unwrap();
        info!("pushed preblock");
    }
}

#[rpc(server)]
pub trait Api {
    #[method(name = "submit_transaction")]
    async fn submit_transaction(
        &self,
        tx: Transaction,
    ) -> Result<TransactionStatus, ErrorObjectOwned>;

    #[method(name = "submit_preblock")]
    async fn submit_preblock(&self, pb: PreBlock) -> Result<(), ErrorObjectOwned>;

    #[method(name = "submit_key_share")]
    async fn submit_key_share(&self, ks: KeyShare) -> Result<(), ErrorObjectOwned>;
}

struct RpcImpl {
    client: Client,
    key_holder_addrs: Vec<String>,
    preblocks: Arc<Mutex<PreBlocks>>,
    receiver: Receiver<PreBlock>,
    key_holders: Arc<Mutex<Vec<Option<Arc<Client>>>>>,
    // key_shares: the outer Vec is for the block levels
    // the middle vec is number of key shares collected for all encrypted txs for the block level
    // the inner vec is for the key shares for individual transactions in a block
    key_shares: Arc<Mutex<Vec<Vec<Vec<DecryptionShare<Bls12_381>>>>>>,
    pending_preblocks: Arc<Mutex<Vec<PreBlock>>>,
    pending_key_shares: Arc<Mutex<Vec<KeyShare>>>,
    setup: Arc<Setup<Bls12_381>>,
    index: u16,
}

async fn mk_client(url: String) -> Option<Arc<Client>> {
    match WsClientBuilder::default().build(url).await {
        Ok(c) => Some(Arc::new(c)),
        Err(_) => None,
    }
}

async fn new(c: Client, x: &[IpAddr], setup: Setup<Bls12_381>, index: u16) -> RpcImpl {
    let preblocks: PreBlocks = PreBlocks::new();
    let receiver: Receiver<PreBlock> = preblocks.receiver.clone();
    let n_clients: usize = 6;
    let key_holders =
        (0..n_clients).map(|i| mk_client(format!("ws://{}:64001", x.get(i).unwrap())));
    let key_holders: Vec<Option<Arc<Client>>> = futures::future::join_all(key_holders).await;
    let key_holder_addrs: Vec<String> = x.iter().cloned().map(|ip| ip.to_string()).collect();

    RpcImpl {
        client: c,
        key_holder_addrs,
        preblocks: Arc::new(Mutex::new(preblocks)),
        receiver,
        key_holders: Arc::new(Mutex::new(key_holders)),
        key_shares: Arc::new(Mutex::new(vec![])),
        pending_preblocks: Arc::new(Mutex::new(vec![])),
        pending_key_shares: Arc::new(Mutex::new(vec![])),
        setup: Arc::new(setup),
        index,
    }
}

#[async_trait]
impl ApiServer for RpcImpl {
    async fn submit_transaction(
        &self,
        tx: Transaction,
    ) -> Result<TransactionStatus, ErrorObjectOwned> {
        info!("submitting tx...");
        //tokio::time::sleep(ENCRYPTION_TIME).await;

        let rng = &mut ark_std::test_rng();

        let start = Instant::now();

        let ciphertext: Ciphertext<Bls12_381> =
            encrypt::<_, ark_bls12_381::Bls12_381>(tx.as_slice(), self.setup.pubkey, rng);

        let duration = start.elapsed();
        info!("tx encrypted! duration {:?}", duration);

        // TODO alter logic
        // process that decrypts blueprints
        // then  checks if transaction was sequenced

        let a = async {
            let start: Instant = Instant::now();
            let mut found = false;
            loop {
                let pbs: Vec<PreBlock> = self.preblocks.lock().await.elements.lock().await.clone();
                let mut level: usize = 0;
                let mut pos: usize = 0;
                for pb in pbs.iter().rev().take(10) {
                    for tx in pb.clone().1 {
                        if tx == ciphertext {
                            info!("tx sequenced! len = {}", pbs.len());
                            found = true;
                            break;
                        }
                        pos += 1;
                    }
                    if found {
                        level = pb.0;
                        break;
                    }
                    pos = 0;
                }
                if found {
                    let duration: Duration = start.elapsed();

                    println!("Time elapsed in transaction_found is: {:?}", duration);
                    break Ok::<(usize, usize), ClientError>((level, pos));
                }
            }
        };

        let (res, _): (Result<(usize, usize), Error>, Result<(), Error>) = join!(
            a,
            self.client.request::<(), ArrayParams>(
                "sequence_transaction",
                rpc_params![ciphertext.clone()]
            )
        );
        let (level, pos): (usize, usize) = res.unwrap();
        info!("done tx! level {}, pos={}", level, pos);

        while self.key_shares.lock().await.get(level).is_none() {}

        let threshold = 4;

        while self.key_shares.lock().await.get(level).unwrap().len() < threshold {}

        info!("decrypting tx");
        //tokio::time::sleep(COMBINE_TIME).await;

        let start = Instant::now();

        let now = Instant::now();
        let shares: Vec<Vec<DecryptionShare<Bls12_381>>> =
            self.key_shares.lock().await.get(level).unwrap().to_vec();
        let tx_shares: Vec<DecryptionShare<Bls12_381>> =
            shares.iter().map(|s| s[pos].clone()).collect();

        let duration = now.elapsed();
        info!(
            "processing key shares for decryption! duration {:?}",
            duration
        );

        let now = Instant::now();
        let prepared_blinded_key_shares: Vec<G2Prepared<ark_bls12_381::Config>> =
            self.setup.contexts[0].prepare_combine(&tx_shares);
        let duration = now.elapsed();
        info!("prepared_blinded_key_shares duration {:?}", duration);

        let now = Instant::now();
        let s: Fp12<ark_bls12_381::Fq12Config> =
            self.setup.contexts[0].share_combine(&tx_shares, &prepared_blinded_key_shares);
        let duration = now.elapsed();
        info!("share_combine duration {:?}", duration);

        let now = Instant::now();
        let plaintext: Vec<u8> = decrypt_with_shared_secret(&ciphertext, &s);
        let duration = now.elapsed();
        info!("decrypt_with_shared_secret duration {:?}", duration);

        let duration = start.elapsed();
        info!("tx decrypted! duration {:?}", duration);

        assert_eq!(plaintext, tx);

        Ok(TransactionStatus::Passed)
    }

    async fn submit_preblock(&self, pb: PreBlock) -> Result<(), ErrorObjectOwned> {
        self.pending_preblocks.lock().await.push(pb);
        Ok(())
    }

    async fn submit_key_share(&self, ks: KeyShare) -> Result<(), ErrorObjectOwned> {
        self.pending_key_shares.lock().await.push(ks);
        Ok(())
    }
}

async fn process_preblocks(
    pending_preblocks: Arc<Mutex<Vec<PreBlock>>>,
    key_holders: Arc<Mutex<Vec<Option<Arc<Client>>>>>,
    preblocks: Arc<Mutex<PreBlocks>>,
    key_holder_addrs: Vec<String>,
    context: PrivateDecryptionContextSerde<Bls12_381>,
) {
    loop {
        match pending_preblocks.lock().await.pop() {
            None => (),
            Some(pb) => {
                let start: Instant = Instant::now();
                let pb_len = pb.1.len();
                info!("Received preblock of length: {}", pb_len);
                // check: It must be the first time it receives a preblock for this height

                // check: They must have applied the blueprint for the previous height.

                preblocks.lock().await.push(pb.0, pb.1.clone()).await;
                let block_level: usize = pb.0;
                info!("sending key shares");
                // send key shares
                let mut khs: MutexGuard<Vec<Option<Arc<Client>>>> = key_holders.lock().await;

                let kh_addrs: Vec<String> = key_holder_addrs.clone();
                let context = context.clone();
                let txs = pb.1;

                let new_khs: Arc<Mutex<Vec<Option<Arc<Client>>>>> =
                    Arc::new(Mutex::new(vec![None; 6]));
                let _ = futures::stream::iter::<&Vec<Option<Arc<Client>>>>(&khs)
                    .enumerate()
                    .for_each(|(i, kh)| {
                        let new_khs: Arc<Mutex<Vec<Option<Arc<Client>>>>> = new_khs.clone();
                        let kh_addrs: Vec<String> = kh_addrs.clone();
                        let context = context.clone();
                        let txs = txs.clone();
                        async move {
                            let kh_addrs: Vec<String> = kh_addrs.clone();
                            let context = context.clone();
                            let txs = txs.clone();
                            let addr: &String = kh_addrs.get(i).unwrap();
                            match kh {
                                None => {
                                    let kh: Option<Arc<Client>> = mk_client(format!("ws://{}:64001", addr)).await;
                                    match kh {
                                        Some(kh) => {
                                            info!("connected to {}", addr);


                                            let start = Instant::now();


                                            let ks: Vec<DecryptionShare<Bls12_381>> = txs.iter().map(|c| context.create_share(&c)).collect();

                                            let ks: KeyShare = (block_level, ks);

                                            let duration = start.elapsed();
                                            info!("key shares generated for preblock of len {}! duration {:?}", pb_len, duration);
                                            let () = kh
                                                .request::<(), ArrayParams>(
                                                    "submit_key_share",
                                                    rpc_params![ks],
                                                )
                                                .await
                                                .unwrap();
                                            let mut new_khs: MutexGuard<Vec<Option<Arc<Client>>>> = new_khs.lock().await;
                                            new_khs[i] = Some(kh.clone());
                                        }
                                        None => {
                                            info!("could not connect to {}", addr)
                                        }
                                    }
                                }
                                Some(kh) => {
                                    info!("already connected to {}", addr);
                                    let kh: Arc<Client> = kh.clone();
                                    tokio::task::spawn(async move {
                                        // Warning! we generate several key shares!!!

                                        let start = Instant::now();

                                        let ks: Vec<DecryptionShare<Bls12_381>> = txs.iter().map(|c| context.create_share(&c)).collect();

                                        let ks: KeyShare = (block_level, ks);
                                        let duration = start.elapsed();
                                        info!("key shares generated for preblock of len {}! duration {:?}", pb_len, duration);
                                        _ = kh
                                            .request::<(), ArrayParams>(
                                                "submit_key_share",
                                                rpc_params![ks],
                                            )
                                            .await
                                    });
                                }
                            };
                        }
                    })
                    .await;

                let new_khs: MutexGuard<Vec<Option<Arc<Client>>>> = new_khs.lock().await;
                for (i, nkh) in new_khs.iter().enumerate() {
                    if let Some(nkh) = nkh {
                        khs[i] = Some(nkh.clone());
                    }
                }

                info!("key shares sent!");

                let duration: Duration = start.elapsed();

                println!(
                    "Time elapsed in process_preblocks iteration is: {:?}",
                    duration
                );
            }
        };
    }
}

async fn process_key_shares(
    pending_key_shares: Arc<Mutex<Vec<KeyShare>>>,
    key_shares: Arc<Mutex<Vec<Vec<Vec<DecryptionShare<Bls12_381>>>>>>,
) {
    loop {
        match pending_key_shares.lock().await.pop() {
            None => (),
            Some(ks) => {
                let start = Instant::now();
                let block_level = ks.0;
                info!("got key share for lvl {}", block_level);

                let mut binding: MutexGuard<Vec<Vec<Vec<DecryptionShare<Bls12_381>>>>> =
                    key_shares.lock().await;
                binding.resize(block_level + 1, vec![]);

                // verify share prior to its inclusion
                tokio::time::sleep(SHARE_VERIFY_TIME).await;
                info!(
                    "got {} key shares for lvl {}",
                    binding.get(block_level).unwrap().len(),
                    block_level
                );

                //
                {
                    assert_eq!(binding.len(), block_level + 1);
                    let x = binding.get_mut(block_level).unwrap();

                    x.push(ks.1);
                }

                let duration = start.elapsed();

                println!(
                    "Time elapsed in process_key_shares iteration is: {:?}",
                    duration
                );
            }
        }
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();
    let Args {
        host,
        port,
        ips,
        index,
    } = Args::parse();
    let sequencer_ip = ips.first().unwrap();
    let key_holder_ips = &ips[1..];

    let output = Command::new("/bin/ls")
        .arg("-ahl")
        .arg("/usr/local/bin/")
        .output()
        .expect("failed to execute process");

    println!("status: {}", output.status);
    println!("stdout: {}", String::from_utf8_lossy(&output.stdout));

    let output = Command::new("/bin/ls")
        .arg("-ahl")
        .arg("/usr/local/bin")
        .output()
        .expect("failed to execute process");

    println!("status: {}", output.status);
    println!("stdout: {}", String::from_utf8_lossy(&output.stdout));

    let setup = fs::read("/usr/local/bin/setup").expect("Unable to read file");
    let setup: Setup<Bls12_381> = Setup::deserialize_compressed(setup.as_slice()).unwrap();

    let () = run_server(&host, port, sequencer_ip, key_holder_ips, setup, index).await;

    Ok(())
}

async fn run_server(
    host: &IpAddr,
    port: u16,
    sequencer_address: &IpAddr,
    key_holder_addresses: &[IpAddr],
    setup: Setup<Bls12_381>,
    index: u16,
) {
    use hyper::service::{make_service_fn, service_fn};

    let addr = SocketAddr::from((*host, port));

    // This state is cloned for every connection
    // all these types based on Arcs and it should
    // be relatively cheap to clone them.
    //
    // Make sure that nothing expensive is cloned here
    // when doing this or use an `Arc`.
    #[derive(Clone)]
    struct PerConnection {
        methods: Methods,
        stop_handle: StopHandle,
        conn_id: Arc<AtomicU32>,
        conn_guard: ConnectionGuard,
    }

    // Each RPC call/connection get its own `stop_handle`
    // to able to determine whether the server has been stopped or not.
    //
    // To keep the server running the `server_handle`
    // must be kept and it can also be used to stop the server.
    let (stop_handle, _server_handle) = stop_channel();

    let sequencer_url = format!("ws://{}:64001", sequencer_address);

    let mut client = WsClientBuilder::default().build(&sequencer_url).await;
    while let Err(e) = client {
        tracing::warn!(
            "Connection to sequencer at {} failed: retrying in 2 seconds - {}",
            sequencer_url,
            e
        );
        sleep(Duration::from_secs(2)).await;
        client = WsClientBuilder::default().build(&sequencer_url).await;
    }

    let client = client.unwrap();

    let state = new(client, key_holder_addresses, setup, index).await;

    let rpc = RpcImpl {
        client: state.client,
        key_holder_addrs: state.key_holder_addrs.clone(),
        preblocks: state.preblocks.clone(),
        receiver: state.receiver.clone(),
        key_holders: state.key_holders.clone(),
        key_shares: state.key_shares.clone(),
        pending_preblocks: state.pending_preblocks.clone(),
        pending_key_shares: state.pending_key_shares.clone(),
        setup: state.setup.clone(),
        index: state.index,
    };

    tokio::task::spawn(async move {
        process_preblocks(
            state.pending_preblocks.clone(),
            state.key_holders.clone(),
            state.preblocks.clone(),
            state.key_holder_addrs.clone(),
            state.setup.contexts[state.index as usize].clone(),
        )
        .await
    });

    tokio::task::spawn(async move {
        process_key_shares(state.pending_key_shares.clone(), state.key_shares.clone()).await
    });

    let per_conn = PerConnection {
        methods: rpc.into_rpc().into(),
        stop_handle: stop_handle.clone(),
        conn_id: Default::default(),
        conn_guard: ConnectionGuard::new(100),
    };

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(move |_conn: &AddrStream| {
        let per_conn = per_conn.clone();

        async move {
            Ok::<_, Box<dyn StdError + Send + Sync>>(service_fn(move |req| {
                let is_websocket = ws::is_upgrade_request(&req);
                let PerConnection {
                    methods,
                    stop_handle,
                    conn_id,
                    conn_guard,
                } = per_conn.clone();

                // jsonrpsee expects a `conn permit` for each connection.
                //
                // This may be omitted if don't want to limit the number of connections
                // to the server.
                let Some(conn_permit) = conn_guard.try_acquire() else {
                    return async {
                        Ok::<_, Box<dyn StdError + Send + Sync>>(response::too_many_requests())
                    }
                    .boxed();
                };

                if is_websocket {
                    let rpc_service = RpcServiceBuilder::new();

                    let conn = ConnectionState::new(
                        stop_handle.clone(),
                        conn_id.fetch_add(1, Ordering::Relaxed),
                        conn_permit,
                    );

                    // Establishes the websocket connection
                    async move {
                        match ws::connect(
                            req,
                            ServerConfig::default(),
                            methods.clone(),
                            conn,
                            rpc_service,
                        )
                        .await
                        {
                            Ok((rp, conn_fut)) => {
                                tokio::spawn(conn_fut);
                                Ok(rp)
                            }
                            Err(rp) => Ok(rp),
                        }
                    }
                    .boxed()
                } else if !ws::is_upgrade_request(&req) {
                    let rpc_service = RpcServiceBuilder::new();

                    let server_cfg = ServerConfig::default();
                    let conn = ConnectionState::new(
                        stop_handle,
                        conn_id.fetch_add(1, Ordering::Relaxed),
                        conn_permit,
                    );

                    // There is another API for making call with just a service as well.
                    //
                    // See [`jsonrpsee::server::http::call_with_service`]
                    async move {
                        // Rpc call finished successfully.
                        Ok(jsonrpsee::server::http::call_with_service_builder(
                            req,
                            server_cfg,
                            conn,
                            methods,
                            rpc_service,
                        )
                        .await)
                    }
                    .boxed()
                } else {
                    async { Ok(http::response::Response::default()) }.boxed()
                }
            }))
        }
    });

    if let Err(err) = hyper::Server::bind(&addr).serve(make_service).await {
        eprintln!("server error: {}", err);
    }
}

#[cfg(test)]
mod tests {
    use std::ops::{Div, Mul, MulAssign};

    use ark_bls12_381::Parameters;
    use ark_bls12_381::{Bls12_381, Fq12Parameters, Fr, FrParameters};
    use ark_ec::bls12::G2Prepared;
    use ark_ec::short_weierstrass_jacobian::GroupProjective;
    use ark_ec::ProjectiveCurve;
    use ark_ff::{Fp12, Fp256, One, PrimeField, Zero};
    use ark_poly::univariate::DensePolynomial;
    use ark_poly::{EvaluationDomain, Polynomial, UVPolynomial};
    use measure_time::print_time;

    use ferveo::batch_to_projective;
    use ferveo::Message;
    use ferveo::Params;
    use ferveo::PubliclyVerifiableDkg;
    use ferveo_common::Keypair;
    use ferveo_common::TendermintValidator;
    use ferveo_common::ValidatorSet;

    // TODO:  initial publicly verifiable DKG phase
    // each key holder:
    // - computes its keypair, validator info
    // - broadcasts its validator info => needs RPC and event loop
    // - runs setup_dkg upon reception of all validators' info
    // - broadcasts the output of dkg.share(rng) to all validators ==> needs transcript RPC & event loop
    // - upon reception of all transcripts, does ss.verify_full(&dkg, rng) & apply_message as in setup_dealt_dkg

    /// Generate a set of keypairs for each validator
    /// DKG.GenerateEpochKeyPair
    pub fn gen_keypairs<EllipticCurve: ark_ec::PairingEngine>(
        num: u64,
    ) -> Vec<ferveo_common::Keypair<EllipticCurve>> {
        let rng = &mut ark_std::test_rng();
        (0..num)
            .map(|_| ferveo_common::Keypair::<EllipticCurve>::new(rng))
            .collect()
    }

    /// Generate a few validators
    pub fn gen_validators<EllipticCurve: ark_ec::PairingEngine>(
        keypairs: &[ferveo_common::Keypair<EllipticCurve>],
    ) -> ValidatorSet<EllipticCurve> {
        let v = (0..keypairs.len())
            .map(|i| TendermintValidator {
                power: 5 / keypairs.len() as u64, // TODO remove hardcoded constant 5
                address: format!("validator_{}", i),
                public_key: keypairs[i].public(),
            })
            .collect::<Vec<_>>();
        ValidatorSet::new(v)
    }

    pub fn lagrange_poly(
        j: usize,
        indices: &Vec<usize>,
        domain_points: &Vec<Fr>,
    ) -> DensePolynomial<Fr> {
        let mut p = DensePolynomial::from_coefficients_vec(vec![Fr::one()]);
        for i in indices {
            if *i == j {
                continue;
            }
            let d = DensePolynomial::from_coefficients_vec(vec![
                domain_points[j].clone() - domain_points[*i].clone(),
            ]);
            let n =
                DensePolynomial::from_coefficients_vec(vec![-domain_points[*i].clone(), Fr::one()]);
            let poly = n.div(&d);
            println!("one= {}, poly {:?}", Fr::one(), poly);
            p = p.mul(&n.div(&d));
        }
        p
    }

    // taken and adapted from https://github.com/anoma/ferveo/commit/8ecdb48c83ead0ddbb57a295c7732893b5a566fb
    #[test]
    pub fn test_pvdkg_tpke() {
        use crate::*;
        use ark_bls12_381::Parameters;
        use ark_ec::bls12::Bls12;
        use ark_ec::AffineCurve;
        use ark_ec::PairingEngine;
        use ark_ff::Field;
        use ark_ff::One;
        use ark_std::UniformRand;
        use group_threshold_cryptography::*;
        //let rng = &mut ark_std::test_rng();
        //let ed_rng = &mut rand::rngs::StdRng::from_seed([0u8; 32]);

        let threshold = 5 * 2 / 3;
        let shares_num = 5;
        let num_entities = 5;

        let num: usize = num_entities;
        // PVDKG phase
        let params = Params {
            tau: 0u64,
            security_threshold: threshold,
            total_weight: shares_num,
            retry_after: 2,
        };

        // todo: generate "keypairs" for validators + "validators"
        // todo: define "me"
        let keypairs: Vec<Keypair<Bls12_381>> = gen_keypairs(num as u64);
        let validators = gen_validators(&keypairs);

        // for _ in 0..1 {
        let mut contexts: Vec<PubliclyVerifiableDkg<Bls12_381>> = vec![];
        for validator in 0..num {
            let me = validators.validators[validator].clone();
            contexts.push(
                PubliclyVerifiableDkg::<ark_bls12_381::Bls12_381>::new(
                    validators.clone(),
                    params,
                    me,
                    keypairs[validator].clone(),
                )
                .unwrap(),
            );
        }

        let rng = &mut ark_std::test_rng();
        // gather everyone's transcripts
        let mut transcripts: Vec<Message<Bls12_381>> = vec![];

        for participant in contexts.iter_mut() {
            println!("hello participant");
            let msg = participant.share(rng).unwrap();
            let msg: Message<ark_bls12_381::Bls12_381> = msg; //.verify().unwrap().1;
            transcripts.push(msg); // do as in setup_dealt_dkg()
        }

        for (sender, pvss) in transcripts.into_iter().rev().enumerate() {
            for node in contexts.iter_mut() {
                println!("hello node");
                //node.apply_message(msg.0, &msg.1.clone()).unwrap();
                if let Message::Deal(ss) = pvss.clone() {
                    print_time!("PVSS verify pvdkg");
                    // Verify individual PVSS message: DKG.VerifyPVSS with PVSS from DKG and new PVSS_i
                    ss.verify_full(&node, rng);
                }
                // AggregatePVSS+VerifyAggregatedPVSS:
                node.apply_message(
                    node.validators[num as usize - 1 - sender].validator.clone(),
                    pvss.clone(),
                )
                .expect("Setup failed");
            }
        }
        println!("len before {}", contexts[1].local_shares.len());

        let mut aggregated_pvss: Vec<ferveo::dkg::Message<Bls12<ark_bls12_381::Parameters>>> =
            vec![];
        for node in contexts.iter_mut() {
            aggregated_pvss.push(node.aggregate().expect("aggregation failed"));
        }

        for node in contexts.iter_mut() {
            println!("Node state is {:?}", node.state);
        }

        for pvss in &aggregated_pvss {
            if let Message::Aggregate(ferveo::Aggregation { vss, .. }) = pvss.clone() {
                println!("public key in agg pvss = {:?}", vss.coeffs[0]);
            }
        }

        for (sender, pvss) in aggregated_pvss.iter().rev().enumerate() {
            for node in contexts.iter_mut() {
                if let Message::Aggregate(ferveo::Aggregation { vss, .. }) = pvss.clone() {
                    println!("cool aggregate {}", sender);
                    vss.verify_aggregation(&node, rng)
                        .expect("verify aggregation failed");
                }
                match node.state {
                    ferveo::DkgState::Success { final_key: _ } => {}
                    _ => {
                        node.apply_message(
                            node.validators[num as usize - 1 - sender].validator.clone(),
                            pvss.clone(),
                        )
                        .expect("Setup failed");
                    }
                }
            }
        }
        for node in contexts.iter_mut() {
            println!("Node state is {:?}", node.state);
        }

        for ctx in contexts.iter_mut() {
            ctx.update_local_state(&aggregated_pvss);
        }

        let g =
            <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G1Affine::prime_subgroup_generator(
            );
        let h =
            <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Affine::prime_subgroup_generator(
            );

        for i in 0..contexts.len() {
            let mut private_key_shares = batch_to_projective(&contexts[i].local_shares);
            let dk_inv = keypairs[i].decryption_key.inverse().unwrap();
            println!(
                "i={}, keypair={:?}, session_keypair {:?}",
                i, keypairs[i], contexts[i].session_keypair
            );
            assert_eq!(
                keypairs[i].public().encryption_key,
                h.mul(keypairs[i].decryption_key)
            );
            assert_eq!(
                contexts[i].session_keypair.decryption_key,
                keypairs[i].decryption_key
            );
            private_key_shares
                .iter_mut()
                .for_each(|s| s.mul_assign(dk_inv));
            let private_key_share: Vec<
                <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Affine,
            > = private_key_shares.iter().map(|s| s.into_affine()).collect();

            let lhs = Bls12_381::pairing(contexts[i].public_key_shares[0], h);
            let rhs = Bls12_381::pairing(g, private_key_share[0]);
            println!("eq?? {}, i = {}", lhs == rhs, i);
        }

        let tpke_pubkey = contexts[0].final_key();
        println!("final tpke_pubkey {:?}", tpke_pubkey);

        println!("len after {}", contexts[1].local_shares.len());

        println!("len after {}", contexts[1].public_key_shares.len());

        println!(
            "threshold {}; {}",
            threshold, contexts[1].params.security_threshold
        );

        ///////////////////////////////////////// TPKE /////////////////////////////////////////

        // Setup TPKE

        //let threshold = contexts[0].params.security_threshold as usize;
        let shares_num = contexts[0].params.total_weight as usize;
        println!("shares num = {}", shares_num);
        let num_entities = contexts.len();
        let msg: &[u8] = "abcdefghijklmnopqrstuvwxyz".as_bytes();

        let rng = &mut ark_std::test_rng();

        let window_size = ark_ec::msm::FixedBaseMSM::get_mul_window_size(100);
        let scalar_bits = <Bls12<ark_bls12_381::Parameters> as PairingEngine>::Fr::size_in_bits();
        let g =
            <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G1Affine::prime_subgroup_generator(
            );
        let h =
            <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Affine::prime_subgroup_generator(
            );

        let mut private_contexts: Vec<PrivateDecryptionContext<Bls12_381>> = vec![];

        // We do not use the public contexts yet (used for key share verification)
        let mut public_contexts: Vec<PublicDecryptionContext<Bls12_381>> = vec![];

        let fft_domain = ark_poly::Radix2EvaluationDomain::<
            <Bls12<ark_bls12_381::Parameters> as PairingEngine>::Fr,
        >::new(shares_num)
        .unwrap();
        let mut domain_points: Vec<Fp256<FrParameters>> = Vec::with_capacity(shares_num);
        let mut point = <Bls12<ark_bls12_381::Parameters> as PairingEngine>::Fr::one();
        let mut domain_points_inv = Vec::with_capacity(shares_num);
        let mut point_inv = <Bls12<ark_bls12_381::Parameters> as PairingEngine>::Fr::one();

        for _ in 0..shares_num {
            domain_points.push(point);
            point *= fft_domain.group_gen;
            domain_points_inv.push(point_inv);
            point_inv *= fft_domain.group_gen_inv;
        }

        println!("nb participants {}", contexts.len());

        let len = fft_domain.size;
        println!("fft len {}", len);

        assert!(shares_num >= threshold as usize);

        for ctx in &contexts {
            assert_eq!(ctx.pvss_params.g, g);
            assert_eq!(g, <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G1Affine::prime_subgroup_generator());
        }

        let mut c = 0;

        for (i, context) in contexts.iter().enumerate() {
            print!(
                "sz {} ; {}\n",
                context.local_shares.to_vec().len(),
                context.public_key_shares.to_vec().len()
            );
            let len = context.local_shares.to_vec().len();
            let domain = &domain_points[c..c + len];
            let domain_inv = &domain_points_inv[c..c + len];
            assert_eq!(domain.len(), len);
            assert_eq!(len, domain_inv.len());
            c += len;

            let b = <Bls12<ark_bls12_381::Parameters> as PairingEngine>::Fr::rand(rng);

            let mut private_key_shares = batch_to_projective(&context.local_shares);
            let dk_inv = keypairs[i].decryption_key.inverse().unwrap();
            println!(
                "i={}, keypair={:?}, session_keypair {:?}",
                i, keypairs[i], context.session_keypair
            );
            assert_eq!(
                keypairs[i].public().encryption_key,
                h.mul(keypairs[i].decryption_key)
            );
            assert_eq!(
                context.session_keypair.decryption_key,
                keypairs[i].decryption_key
            );
            private_key_shares
                .iter_mut()
                .for_each(|s| s.mul_assign(dk_inv));
            let private_key_share = PrivateKeyShare::<ark_bls12_381::Bls12_381> {
                // See part validator decryption of private key shares https://nikkolasg.github.io/ferveo/pvss.html
                private_key_shares: private_key_shares.iter().map(|s| s.into_affine()).collect(),
            };
            let mut blinded_key_shares: BlindedKeyShares<Bls12_381> =
                private_key_share.blind(b.clone());
            blinded_key_shares.multiply_by_omega_inv(domain_inv);

            private_contexts.push(PrivateDecryptionContext::<ark_bls12_381::Bls12_381> {
                index: context.me,
                b,
                b_inv: b.inverse().unwrap(),
                private_key_share,
                public_decryption_contexts: vec![],
                g,
                g_inv: <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G1Prepared::from(-g),
                h_inv: <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Prepared::from(-h),
                scalar_bits,
                window_size,
            });
            let mut lagrange_n_0: <Bls12<Parameters> as PairingEngine>::Fr =
                domain
                    .iter()
                    .product::<<Bls12<ark_bls12_381::Parameters> as PairingEngine>::Fr>();
            if domain.len() % 2 == 1 {
                lagrange_n_0 = -lagrange_n_0;
            }
            public_contexts.push(PublicDecryptionContext::<ark_bls12_381::Bls12_381> {
                domain: domain.to_vec(),
                public_key_shares: PublicKeyShares::<ark_bls12_381::Bls12_381> {
                    public_key_shares: context.public_key_shares.clone(),
                },
                blinded_key_shares,
                lagrange_n_0,
            });
        }
        for private in private_contexts.iter_mut() {
            private.public_decryption_contexts = public_contexts.clone();
        }

        // Let's test the DKG:
        // We retrieve the master secret
        // Compute Lagrange polynomials L_i
        // sk = prod_i private_key_share[i]^L_i(0)

        println!(
            "len public contexts = {}, threshold {}",
            public_contexts.len(),
            threshold
        );
        let indices: Vec<usize> = (0..public_contexts.len()).collect();

        for i in 0..public_contexts.len() {
            //assert_eq!(private_contexts[i].index, i);
            //assert_eq!(contexts[i].me, i);
            let lhs =
                Bls12_381::pairing(public_contexts[i].public_key_shares.public_key_shares[0], h);
            let rhs = Bls12_381::pairing(
                g,
                private_contexts[i].private_key_share.private_key_shares[0],
            );
            println!(
                "eq? {}, i = {}, eq pks = {}",
                lhs == rhs,
                i,
                public_contexts[i].public_key_shares.public_key_shares
                    == contexts[i].public_key_shares
            );
        }

        let sk: GroupProjective<ark_bls12_381::g2::Parameters> = private_contexts
            .iter()
            .enumerate()
            .map(|(i, c)| {
                let l = lagrange_poly(c.index, &indices, &domain_points);
                //assert_eq!(i, c.index);
                println!("index {}", c.index);
                let eval = DensePolynomial::evaluate(&l, &Fr::zero());
                c.private_key_share.private_key_shares[0].mul(eval)
            })
            .fold(
                <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Projective::zero(),
                |acc, x| acc + x,
            );
        // check e(tpke_pubkey, h) == e(g,sk)
        let _lhs: Fp12<ark_bls12_381::Fq12Parameters> = Bls12_381::pairing(tpke_pubkey, h);
        let _rhs: Fp12<ark_bls12_381::Fq12Parameters> = Bls12_381::pairing(g, sk);
        //assert_eq!(_lhs, _rhs);

        // See https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=f613e2a76843153d19adcd7c59f2766334f799bf
        // section 3.2.2 function E(pk)(m)
        // TE + Symmetric encryption
        let ciphertext: Ciphertext<Bls12_381> =
            encrypt::<_, ark_bls12_381::Bls12_381>(msg, tpke_pubkey, rng);

        // create Decryption Shares
        // Generate key shares
        let mut shares: Vec<DecryptionShare<ark_bls12_381::Bls12_381>> = vec![];
        println!("thresh {}", threshold);
        for context in private_contexts.iter() {
            shares.push(context.create_share(&ciphertext));
        }
        println!("len shares= {}", shares.len());
        // Key shares verification
        /*for pub_context in public_contexts.iter() {
            assert!(pub_context
                .blinded_key_shares
                .verify_blinding(&pub_context.public_key_shares, rng));
        }*/

        // TE Combine + Symmetric decryption
        let prepared_blinded_key_shares: Vec<G2Prepared<Parameters>> =
            private_contexts[0].prepare_combine(&shares);
        let s: Fp12<Fq12Parameters> =
            private_contexts[0].share_combine(&shares, &prepared_blinded_key_shares);

        let plaintext = decrypt_with_shared_secret(&ciphertext, &s);
        println!("plaintext={:?}", plaintext);
        println!("plaintext={:?}", std::str::from_utf8(&plaintext));
        println!("msg={:?}", msg);
        assert_eq!(plaintext, msg)
    }
}
