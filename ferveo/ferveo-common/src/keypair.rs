use ark_ec::pairing::Pairing;
use ark_ec::AffineRepr;
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize};
use serde::*;
use std::ops::Mul;

#[derive(Copy, Clone, Debug)]
pub struct PreparedPublicKey<E: Pairing> {
    pub encryption_key: E::G2Prepared,
}

impl<E: Pairing> From<PublicKey<E>> for PreparedPublicKey<E> {
    fn from(value: PublicKey<E>) -> Self {
        PreparedPublicKey::<E> {
            encryption_key: E::G2Prepared::from(value.encryption_key),
        }
    }
}

#[derive(
    Copy,
    Clone,
    Debug,
    PartialEq,
    Serialize,
    Deserialize,
    CanonicalSerialize,
    CanonicalDeserialize,
)]
pub struct PublicKey<E: Pairing> {
    #[serde(with = "crate::ark_serde")]
    pub encryption_key: E::G2Affine,
}

impl<E: Pairing> Default for PublicKey<E> {
    fn default() -> Self {
        Self {
            encryption_key: E::G2Affine::generator(),
        }
    }
}

#[derive(
    Clone,
    Copy,
    Debug,
    PartialEq,
    Serialize,
    Deserialize,
    CanonicalSerialize,
    CanonicalDeserialize,
)]
pub struct Keypair<E: Pairing> {
    #[serde(with = "crate::ark_serde")]
    pub decryption_key: E::ScalarField,
}

impl<E: Pairing> Keypair<E> {
    /// Returns the public session key for the publicly verifiable DKG participant
    pub fn public(&self) -> PublicKey<E> {
        PublicKey::<E> {
            encryption_key: E::G2Affine::generator()
                .mul(self.decryption_key)
                .into(),
        }
    }

    /// Creates a new ephemeral session key for participating in the DKG
    pub fn new<R: crate::Rng>(rng: &mut R) -> Self {
        use ark_std::UniformRand;
        Self {
            decryption_key: E::ScalarField::rand(rng),
        }
    }
}
