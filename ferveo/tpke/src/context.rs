use ark_serialize::CanonicalDeserialize;

use crate::*;

#[derive(Clone, Debug)]
pub struct PublicDecryptionContext<E: ark_ec::pairing::Pairing> {
    pub domain: Vec<E::ScalarField>,
    pub public_key_shares: PublicKeyShares<E>,
    pub blinded_key_shares: BlindedKeyShares<E>,
    // This decrypter's contribution to N(0), namely (-1)^|domain| * \prod_i omega_i
    pub lagrange_n_0: E::ScalarField,
}

#[derive(Clone, PartialEq, Debug, CanonicalSerialize, CanonicalDeserialize)]
pub struct PublicDecryptionContextSerde<E: ark_ec::pairing::Pairing> {
    pub domain: Vec<E::ScalarField>,
    pub public_key_shares: PublicKeyShares<E>,
    pub blinded_key_shares: BlindedKeySharesSerde<E>,
    // This decrypter's contribution to N(0), namely (-1)^|domain| * \prod_i omega_i
    pub lagrange_n_0: E::ScalarField,
}

pub fn new_public_decryption_context<E: ark_ec::pairing::Pairing>(
    ctx: &PublicDecryptionContext<E>,
) -> PublicDecryptionContextSerde<E> {
    PublicDecryptionContextSerde {
        domain: ctx.domain.clone(),
        public_key_shares: ctx.public_key_shares.clone(),
        blinded_key_shares: new_blinded_key_shares(&ctx.blinded_key_shares),
        // This decrypter's contribution to N(0), namely (-1)^|domain| * \prod_i omega_i
        lagrange_n_0: ctx.lagrange_n_0,
    }
}

#[derive(Debug)]
pub struct PrivateDecryptionContext<E: ark_ec::pairing::Pairing> {
    pub index: usize,
    pub b: E::ScalarField,
    pub b_inv: E::ScalarField,
    pub private_key_share: PrivateKeyShare<E>,
    pub public_decryption_contexts: Vec<PublicDecryptionContext<E>>,
    pub g: E::G1Affine,
    pub g_inv: E::G1Prepared,
    pub h_inv: E::G2Prepared,
    pub scalar_bits: usize,
    pub window_size: usize,
}

#[derive(Clone, Debug, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct PrivateDecryptionContextSerde<E: ark_ec::pairing::Pairing> {
    pub index: usize,
    pub b: E::ScalarField,
    pub b_inv: E::ScalarField,
    pub private_key_share: PrivateKeyShare<E>,
    pub public_decryption_contexts: Vec<PublicDecryptionContextSerde<E>>,
    pub g: E::G1Affine,
    pub g_inv: E::G1Affine,
    pub h_inv: E::G2Affine,
    pub scalar_bits: usize,
    pub window_size: usize,
}

pub fn new_private_decryption_context<E: ark_ec::pairing::Pairing>(
    context: &PrivateDecryptionContext<E>,
) -> PrivateDecryptionContextSerde<E> {
    let mut g_inv: Vec<u8> = Vec::new();
    context
        .g_inv
        .serialize_compressed(&mut g_inv)
        .expect("Failed to write to bytes");
    let mut h_inv: Vec<u8> = Vec::new();
    context
        .h_inv
        .serialize_compressed(&mut h_inv)
        .expect("Failed to write to bytes");
    PrivateDecryptionContextSerde {
        index: context.index,
        b: context.b,
        b_inv: context.b_inv,
        private_key_share: context.private_key_share.clone(),
        public_decryption_contexts: context
            .public_decryption_contexts
            .iter()
            .map(|c| new_public_decryption_context(c))
            .collect(),
        g: context.g,
        g_inv: E::G1Affine::from_random_bytes(g_inv.as_slice())
            .expect("Failed to read from bytes"),
        h_inv: E::G2Affine::from_random_bytes(h_inv.as_slice())
            .expect("Failed to read from bytes"),
        scalar_bits: context.scalar_bits,
        window_size: context.window_size,
    }
}
