#![allow(non_snake_case)]
#![allow(dead_code)]

use crate::*;
use ark_ec::AffineRepr;
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize};
use itertools::Itertools;

#[derive(Clone, Debug, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct PublicKeyShares<E: ark_ec::pairing::Pairing> {
    pub public_key_shares: Vec<E::G1Affine>, // A_{i, \omega_i}
}

#[derive(Clone, Debug)]
pub struct BlindedKeyShares<E: ark_ec::pairing::Pairing> {
    pub blinding_key: E::G2Affine,            // [b] H
    pub blinding_key_prepared: E::G2Prepared, // [b] H
    pub blinded_key_shares: Vec<E::G2Affine>, // [b] Z_{i, \omega_i}
    pub window_tables: Vec<BlindedKeyShareWindowTable<E>>, // [b*omega_i^-1] Z_{i, \omega_i}
}

#[derive(Clone, Debug, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct BlindedKeySharesSerde<E: ark_ec::pairing::Pairing> {
    pub blinding_key: E::G2Affine,            // [b] H
    pub blinding_key_prepared: E::G2Affine,   // [b] H
    pub blinded_key_shares: Vec<E::G2Affine>, // [b] Z_{i, \omega_i}
    pub window_tables: Vec<BlindedKeyShareWindowTable<E>>, // [b*omega_i^-1] Z_{i, \omega_i}
}

pub fn new_blinded_key_shares<E: ark_ec::pairing::Pairing>(
    key_shares: &BlindedKeyShares<E>,
) -> BlindedKeySharesSerde<E> {
    let mut blinding_key_prepared: Vec<u8> = Vec::new();
    key_shares
        .blinding_key_prepared
        .serialize_compressed(&mut blinding_key_prepared)
        .expect("Failed to write to bytes");

    BlindedKeySharesSerde {
        blinding_key: key_shares.blinding_key,
        blinding_key_prepared: E::G2Affine::from_random_bytes(
            blinding_key_prepared.as_slice(),
        )
        .expect(""),
        blinded_key_shares: key_shares.blinded_key_shares.clone(),
        window_tables: key_shares.window_tables.clone(),
    }
}

impl<E: ark_ec::pairing::Pairing> BlindedKeyShares<E> {
    // could be https://nikkolasg.github.io/ferveo/tpke.html#public-verification-of-decryption-shares
    // not clear
    // allows checking the validity of key shares, the key shares have to be blinded
    pub fn verify_blinding<R: RngCore>(
        &self,
        public_key_shares: &PublicKeyShares<E>,
        rng: &mut R,
    ) -> bool {
        let g = E::G1Affine::generator();
        let alpha = E::ScalarField::rand(rng);
        /*let alpha_i = generate_random::<_, E>(
            public_key_shares.public_key_shares.len(),
            rng,
        );*/

        let len = public_key_shares.public_key_shares.len();
        let alpha_i: Vec<E::ScalarField> =
            (0..len).map(|i| alpha.pow([i as u64].as_ref())).collect();

        let alpha_a_i = E::G1Prepared::from(
            g + public_key_shares
                .public_key_shares
                .iter()
                .zip_eq(alpha_i.iter())
                .map(|(key, alpha)| key.mul(*alpha))
                .sum::<E::G1>()
                .into_affine(),
        );

        let alpha_z_i = E::G2Prepared::from(
            self.blinding_key
                + self
                    .blinded_key_shares
                    .iter()
                    .zip_eq(alpha_i.iter())
                    .map(|(key, alpha)| key.mul(*alpha))
                    .sum::<E::G2>()
                    .into_affine(),
        );

        E::multi_pairing(
            [E::G1Prepared::from(g.into_group().neg()), alpha_a_i],
            [alpha_z_i, E::G2Prepared::from(self.blinding_key)],
        )
        .0 == E::TargetField::one()
    }

    pub fn get_window_table(
        &self,
        window_size: usize,
        scalar_bits: usize,
        domain_inv: &[E::ScalarField],
    ) -> Vec<BlindedKeyShareWindowTable<E>> {
        izip!(self.blinded_key_shares.iter(), domain_inv.iter())
            .map(|(key, omega_inv)| BlindedKeyShareWindowTable::<E> {
                window_table: FixedBase::get_window_table(
                    scalar_bits,
                    window_size,
                    key.mul(-*omega_inv),
                ),
            })
            .collect::<Vec<_>>()
    }

    pub fn multiply_by_omega_inv(&mut self, domain_inv: &[E::ScalarField]) {
        izip!(self.blinded_key_shares.iter_mut(), domain_inv.iter()).for_each(
            |(key, omega_inv)| *key = key.mul(-*omega_inv).into_affine(),
        )
    }
}

impl<E: ark_ec::pairing::Pairing> BlindedKeySharesSerde<E> {
    pub fn multiply_by_omega_inv(&mut self, domain_inv: &[E::ScalarField]) {
        izip!(self.blinded_key_shares.iter_mut(), domain_inv.iter()).for_each(
            |(key, omega_inv)| *key = key.mul(-*omega_inv).into_affine(),
        )
    }
}

#[derive(Clone, Debug, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct BlindedKeyShareWindowTable<E: ark_ec::pairing::Pairing> {
    pub window_table: Vec<Vec<E::G2Affine>>,
}

#[derive(Clone, Debug, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct PrivateKeyShare<E: ark_ec::pairing::Pairing> {
    pub private_key_shares: Vec<E::G2Affine>,
}

impl<E: ark_ec::pairing::Pairing> PrivateKeyShare<E> {
    pub fn blind(&self, b: E::ScalarField) -> BlindedKeyShares<E> {
        let blinding_key = E::G2Affine::generator().mul(b).into_affine();
        BlindedKeyShares::<E> {
            blinding_key,
            blinding_key_prepared: E::G2Prepared::from(blinding_key),
            blinded_key_shares: self
                .private_key_shares
                .iter()
                .map(|z| z.mul(b).into_affine())
                .collect::<Vec<_>>(),
            window_tables: vec![],
        }
    }

    pub fn blind2(&self, b: E::ScalarField) -> BlindedKeySharesSerde<E> {
        let blinding_key = E::G2Affine::generator().mul(b).into_affine();
        BlindedKeySharesSerde::<E> {
            blinding_key,
            blinding_key_prepared: (blinding_key),
            blinded_key_shares: self
                .private_key_shares
                .iter()
                .map(|z| z.mul(b).into_affine())
                .collect::<Vec<_>>(),
            window_tables: vec![],
        }
    }
}
