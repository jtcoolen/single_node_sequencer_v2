#![allow(non_snake_case)]
#![allow(dead_code)]

use crate::*;
use ark_poly::Radix2EvaluationDomain;

impl<E: ark_ec::pairing::Pairing> PrivateDecryptionContext<E> {
    pub fn prepare_combine(
        &self,
        shares: &[DecryptionShare<E>],
    ) -> Vec<E::G2Prepared> {
        let mut domain = vec![];
        let mut n_0 = E::ScalarField::one();
        for d_i in shares.iter() {
            // for each decryption share D_i=[1/b]U where b is the random blinding factor
            domain.extend(
                self.public_decryption_contexts[d_i.decryptor_index]
                    .domain
                    .iter(),
            );
            n_0 *= self.public_decryption_contexts[d_i.decryptor_index]
                .lagrange_n_0;
        }
        let _: Radix2EvaluationDomain<E::ScalarField> =
            EvaluationDomain::new(256).unwrap();
        let s = SubproductDomain::<E::ScalarField>::new(domain);
        let mut lagrange = s.inverse_lagrange_coefficients();
        ark_ff::batch_inversion_and_mul(&mut lagrange, &n_0);
        let mut start = 0usize;
        // Computes the sum in the second argument of the pairing https://nikkolasg.github.io/ferveo/tpke.html#threshold-decryption-fast-method
        shares
            .iter()
            .map(|d_i| {
                let decryptor =
                    &self.public_decryption_contexts[d_i.decryptor_index];
                let end = start + decryptor.domain.len();
                let lagrange_slice = &lagrange[start..end];
                start = end;
                E::G2Prepared::from(
                    izip!(
                        lagrange_slice.iter(),
                        decryptor.blinded_key_shares.blinded_key_shares.iter() //decryptor.blinded_key_shares.window_tables.iter()
                    )
                    // iterates over the blinded key shares [b]Z_(i,w_j)
                    .map(|(lambda, blinded_key_share)| {
                        blinded_key_share.mul(*lambda)
                    })
                    /*.map(|(lambda, base_table)| {
                        FixedBaseMSM::multi_scalar_mul::<E::G2Projective>(
                            self.scalar_bits,
                            self.window_size,
                            &base_table.window_table,
                            &[*lambda],
                        )[0]
                    })*/
                    .sum::<E::G2>()
                    .into_affine(),
                )
            })
            .collect::<Vec<_>>()
    }

    // see https://nikkolasg.github.io/ferveo/tpke.html#threshold-decryption-fast-method
    pub fn share_combine(
        &self,
        shares: &[DecryptionShare<E>],
        prepared_key_shares: &[E::G2Prepared], // output of prepare_combine
    ) -> E::TargetField {
        /*let mut pairing_product: Vec<(E::G1Prepared, E::G2Prepared)> = vec![];

        for (d_i, blinded_key_share) in
            izip!(shares, prepared_key_shares.iter())
        {
            // e(D_i, [b*omega_i^-1] Z_{i,omega_i})
            pairing_product.push((
                E::G1Prepared::from(d_i.decryption_share),
                blinded_key_share.clone(),
            ));
        }*/
        let d_is: Vec<E::G1Affine> =
            shares.iter().map(|s| s.decryption_share.clone()).collect();
        let blinded_key_shares: Vec<E::G2Prepared> =
            prepared_key_shares.iter().map(|e| e.clone()).collect();
        // s
        E::multi_pairing(d_is, blinded_key_shares).0
    }
}

impl<E: ark_ec::pairing::Pairing> PrivateDecryptionContextSerde<E> {
    pub fn prepare_combine(
        &self,
        shares: &[DecryptionShare<E>],
    ) -> Vec<E::G2Prepared> {
        let mut domain = vec![];
        let mut n_0 = E::ScalarField::one();
        for d_i in shares.iter() {
            // for each decryption share D_i=[1/b]U where b is the random blinding factor
            domain.extend(
                self.public_decryption_contexts[d_i.decryptor_index]
                    .domain
                    .iter(),
            );
            n_0 *= self.public_decryption_contexts[d_i.decryptor_index]
                .lagrange_n_0;
        }
        let s = SubproductDomain::<E::ScalarField>::new(domain);
        let mut lagrange = s.inverse_lagrange_coefficients();
        ark_ff::batch_inversion_and_mul(&mut lagrange, &n_0);
        let mut start = 0usize;
        // Computes the sum in the second argument of the pairing https://nikkolasg.github.io/ferveo/tpke.html#threshold-decryption-fast-method
        shares
            .iter()
            .map(|d_i| {
                let decryptor =
                    &self.public_decryption_contexts[d_i.decryptor_index];
                let end = start + decryptor.domain.len();
                let lagrange_slice = &lagrange[start..end];
                start = end;
                E::G2Prepared::from(
                    izip!(
                        lagrange_slice.iter(),
                        decryptor.blinded_key_shares.blinded_key_shares.iter() //decryptor.blinded_key_shares.window_tables.iter()
                    )
                    // iterates over the blinded key shares [b]Z_(i,w_j)
                    .map(|(lambda, blinded_key_share)| {
                        blinded_key_share.mul(*lambda)
                    })
                    /*.map(|(lambda, base_table)| {
                        FixedBaseMSM::multi_scalar_mul::<E::G2Projective>(
                            self.scalar_bits,
                            self.window_size,
                            &base_table.window_table,
                            &[*lambda],
                        )[0]
                    })*/
                    .sum::<E::G2>()
                    .into_affine(),
                )
            })
            .collect::<Vec<_>>()
    }

    // see https://nikkolasg.github.io/ferveo/tpke.html#threshold-decryption-fast-method
    pub fn share_combine(
        &self,
        shares: &[DecryptionShare<E>],
        prepared_key_shares: &[E::G2Prepared], // output of prepare_combine
    ) -> E::TargetField {
        //let mut pairing_product: Vec<(E::G1Prepared, E::G2Prepared)> = vec![];

        /*for (d_i, blinded_key_share) in
            izip!(shares, prepared_key_shares.iter())
        {
            // e(D_i, [b*omega_i^-1] Z_{i,omega_i})
            pairing_product.push((
                E::G1Prepared::from(d_i.decryption_share),
                blinded_key_share.clone(),
            ));
        }*/
        let d_is: Vec<E::G1Affine> =
            shares.iter().map(|s| s.decryption_share.clone()).collect();
        let blinded_key_shares: Vec<E::G2Prepared> =
            prepared_key_shares.iter().map(|e| e.clone()).collect();
        // s
        E::multi_pairing(d_is, blinded_key_shares).0
    }
}

#[cfg(test)]
mod tests {

    type ScalarField =
        <ark_bls12_381::Bls12_381 as ark_ec::pairing::Pairing>::ScalarField;

    #[test]
    fn test_lagrange() {
        use ark_poly::EvaluationDomain;
        use ark_std::One;
        let fft_domain =
            ark_poly::Radix2EvaluationDomain::<ScalarField>::new(500).unwrap();

        let mut domain = Vec::with_capacity(500);
        let mut point = ScalarField::one();
        for _ in 0..500 {
            domain.push(point);
            point *= fft_domain.group_gen;
        }

        let mut lagrange_n_0 = domain.iter().product::<ScalarField>();
        if domain.len() % 2 == 1 {
            lagrange_n_0 = -lagrange_n_0;
        }
        let s = subproductdomain::SubproductDomain::<ScalarField>::new(domain);
        let mut lagrange = s.inverse_lagrange_coefficients();
        ark_ff::batch_inversion_and_mul(&mut lagrange, &lagrange_n_0);
    }
}
