#![allow(non_snake_case)]
#![allow(dead_code)]

use std::ops::{Mul, Neg};
use std::usize;

use ark_bls12_381::Bls12_381;
// TODO switch to Twisted Edwards
use ark_ec::scalar_mul::fixed_base::FixedBase;
use ark_ec::{pairing::Pairing, AffineRepr, CurveGroup, Group};
use ark_ec::bn::G2Affine;
use ark_ff::{Field, One, PrimeField, UniformRand, Zero};
use ark_poly::EvaluationDomain;
use ark_poly::{univariate::DensePolynomial, DenseUVPolynomial};
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize};
use itertools::izip;
use rand_core::RngCore;
use thiserror::Error;

pub use ciphertext::*;
pub use context::*;
pub use decryption::*;
pub use key_share::*;
use subproductdomain::SubproductDomain;

use crate::hash_to_curve::htp_bls12381_g2;

use crate::hash_to_curve::wb_hash_arbitrary_bytes_to_curve;

mod ciphertext;
mod hash_to_curve;

mod key_share;

mod decryption;

mod combine;
mod context;

pub trait ThresholdEncryptionParameters {
    type E: ark_ec::pairing::Pairing;
}

#[derive(Debug, Error)]
pub enum ThresholdEncryptionError {
    /// Error
    #[error("ciphertext verification failed")]
    CiphertextVerificationFailed,

    /// Error
    #[error("Decryption share verification failed")]
    DecryptionShareVerificationFailed,

    /// Hashing to curve failed
    #[error("Could not hash to curve")]
    HashToCurveError,

    #[error("plaintext verification failed")]
    PlaintextVerificationFailed,
}

fn hash_to_g2<T: ark_serialize::CanonicalDeserialize>(message: &[u8]) -> T {
    let mut point_ser: Vec<u8> = Vec::new();
    //let point = htp_bls12381_g2(message);
    let point : ark_bls12_381::G2Affine = wb_hash_arbitrary_bytes_to_curve(message);
    point.serialize_compressed(&mut point_ser).unwrap();
    T::deserialize_compressed(&point_ser[..]).unwrap()
}

fn construct_tag_hash<E: ark_ec::pairing::Pairing>(
    u: E::G1Affine,
    stream_ciphertext: &[u8],
) -> E::G2Affine {
    let mut hash_input = Vec::<u8>::new();
    u.serialize_compressed(&mut hash_input).unwrap();
    hash_input.extend_from_slice(stream_ciphertext);

    hash_to_g2(&hash_input)
}

pub fn setup<E: ark_ec::pairing::Pairing>(
    threshold: usize,
    shares_num: usize,
    num_entities: usize,
) -> (
    E::G1Affine,
    E::G2Affine,
    Vec<PrivateDecryptionContext<E>>,
    Vec<<E as ark_ec::pairing::Pairing>::ScalarField>,
) {
    let rng = &mut ark_std::test_rng();
    let g = E::G1Affine::generator();
    let h = E::G2Affine::generator();
    let _g_inv = E::G1Prepared::from(g.into_group().neg());
    let _h_inv = E::G2Prepared::from(h.into_group().neg());

    assert!(shares_num >= threshold);
    let threshold_poly =
        DensePolynomial::<E::ScalarField>::rand(threshold - 1, rng);
    println!("threshold = {}, shares_num = {}", threshold, shares_num);
    let fft_domain =
        ark_poly::Radix2EvaluationDomain::<E::ScalarField>::new(shares_num)
            .unwrap();
    let evals = threshold_poly.evaluate_over_domain_by_ref(fft_domain);

    let mut domain_points: Vec<<E as ark_ec::pairing::Pairing>::ScalarField> =
        Vec::with_capacity(shares_num);
    let mut point = E::ScalarField::one();
    let mut domain_points_inv = Vec::with_capacity(shares_num);
    let mut point_inv = E::ScalarField::one();

    for _ in 0..shares_num {
        domain_points.push(point);
        point *= fft_domain.group_gen;
        domain_points_inv.push(point_inv);
        point_inv *= fft_domain.group_gen_inv;
    }

    let window_size = FixedBase::get_mul_window_size(100);
    let scalar_bits = E::ScalarField::MODULUS_BIT_SIZE as usize;

    let pubkey_shares =
        subproductdomain::fast_multiexp(&evals.evals, g.into_group());
    let privkey_shares =
        subproductdomain::fast_multiexp(&evals.evals, E::G2::generator());

    let x = threshold_poly.coeffs[0];
    let pubkey = g.mul(x);
    let privkey = h.mul(x);

    let mut private_contexts = vec![];
    let mut public_contexts = vec![];

    for (index, (domain, domain_inv, public, private)) in izip!(
        domain_points.chunks(shares_num / num_entities),
        domain_points_inv.chunks(shares_num / num_entities),
        pubkey_shares.chunks(shares_num / num_entities),
        privkey_shares.chunks(shares_num / num_entities)
    )
    .enumerate()
    {
        print!(
            "sz {} ; {}\n",
            private.to_vec().len(),
            public.to_vec().len()
        );
        let private_key_share = PrivateKeyShare::<E> {
            private_key_shares: private.to_vec(),
        };
        let b = E::ScalarField::rand(rng);
        let mut blinded_key_shares = private_key_share.blind(b);
        blinded_key_shares.multiply_by_omega_inv(domain_inv);
        /*blinded_key_shares.window_tables =
        blinded_key_shares.get_window_table(window_size, scalar_bits, domain_inv);*/
        private_contexts.push(PrivateDecryptionContext::<E> {
            index,
            b,
            b_inv: b.inverse().unwrap(),
            private_key_share,
            public_decryption_contexts: vec![],
            g,
            g_inv: E::G1Prepared::from(g.into_group().neg()),
            h_inv: E::G2Prepared::from(h.into_group().neg()),
            scalar_bits,
            window_size,
        });
        let mut lagrange_n_0 = domain.iter().product::<E::ScalarField>();
        if domain.len() % 2 == 1 {
            lagrange_n_0 = -lagrange_n_0;
        }
        public_contexts.push(PublicDecryptionContext::<E> {
            domain: domain.to_vec(),
            public_key_shares: PublicKeyShares::<E> {
                public_key_shares: public.to_vec(),
            },
            blinded_key_shares,
            lagrange_n_0,
        });
    }
    for private in private_contexts.iter_mut() {
        private.public_decryption_contexts = public_contexts.clone();
    }

    (
        pubkey.into(),
        privkey.into(),
        private_contexts,
        domain_points,
    )
}

#[derive(Debug, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct Setup<E: ark_ec::pairing::Pairing> {
    pub pubkey: E::G1Affine,
    pub contexts: Vec<PrivateDecryptionContextSerde<E>>,
}

// todo: benchmark executable
// todo: choose most efficient coordinate system (twisted edwards, montgomery,...)
pub fn setup2<E: ark_ec::pairing::Pairing>(
    threshold: usize,
    shares_num: usize,
    num_entities: usize,
) -> Setup<E> {
    let rng = &mut ark_std::test_rng();
    let g: <E as Pairing>::G1Affine = E::G1Affine::generator();
    let h = E::G2Affine::generator();
    let _g_inv = E::G1Prepared::from(g.into_group().neg());
    let _h_inv = E::G2Prepared::from(h.into_group().neg());

    assert!(shares_num >= threshold);
    let threshold_poly =
        DensePolynomial::<E::ScalarField>::rand(threshold - 1, rng);
    println!("threshold = {}, shares_num = {}", threshold, shares_num);
    let fft_domain =
        ark_poly::Radix2EvaluationDomain::<E::ScalarField>::new(shares_num)
            .unwrap();
    let evals = threshold_poly.evaluate_over_domain_by_ref(fft_domain);

    let mut domain_points: Vec<<E as ark_ec::pairing::Pairing>::ScalarField> =
        Vec::with_capacity(shares_num);
    let mut point = E::ScalarField::one();
    let mut domain_points_inv = Vec::with_capacity(shares_num);
    let mut point_inv = E::ScalarField::one();

    for _ in 0..shares_num {
        domain_points.push(point);
        point *= fft_domain.group_gen;
        domain_points_inv.push(point_inv);
        point_inv *= fft_domain.group_gen_inv;
    }

    let window_size = FixedBase::get_mul_window_size(100);
    let scalar_bits = E::ScalarField::MODULUS_BIT_SIZE;

    // todo: check generator from G1==G1Affine
    let pubkey_shares =
        subproductdomain::fast_multiexp(&evals.evals, g.into_group());
    let privkey_shares =
        subproductdomain::fast_multiexp(&evals.evals, E::G2::generator());

    let x: <E as Pairing>::ScalarField = threshold_poly.coeffs[0];
    let pubkey = g.mul(x);
    //let privkey: Projective<G2Cfg> = h.mul(x);

    let mut private_contexts: Vec<PrivateDecryptionContextSerde<E>> = vec![];
    let mut public_contexts: Vec<PublicDecryptionContextSerde<E>> = vec![];

    for (index, (domain, domain_inv, public, private)) in izip!(
        domain_points.chunks(shares_num / num_entities),
        domain_points_inv.chunks(shares_num / num_entities),
        pubkey_shares.chunks(shares_num / num_entities),
        privkey_shares.chunks(shares_num / num_entities)
    )
    .enumerate()
    {
        print!(
            "sz {} ; {}\n",
            private.to_vec().len(),
            public.to_vec().len()
        );
        let private_key_share = PrivateKeyShare::<E> {
            private_key_shares: private.to_vec(),
        };
        let b = E::ScalarField::rand(rng);
        let mut blinded_key_shares: BlindedKeySharesSerde<E> =
            private_key_share.blind2(b);
        blinded_key_shares.multiply_by_omega_inv(domain_inv);
        /*blinded_key_shares.window_tables =
        blinded_key_shares.get_window_table(window_size, scalar_bits, domain_inv);*/
        private_contexts.push(PrivateDecryptionContextSerde::<E> {
            index,
            b,
            b_inv: b.inverse().unwrap(),
            private_key_share,
            public_decryption_contexts: vec![],
            g,
            g_inv: (E::G1Affine::into_group(g).neg().into_affine()),
            h_inv: (E::G2Affine::into_group(h).neg().into_affine()),
            scalar_bits: scalar_bits as usize,
            window_size,
        });
        let mut lagrange_n_0 = domain.iter().product::<E::ScalarField>();
        if domain.len() % 2 == 1 {
            lagrange_n_0 = -lagrange_n_0;
        }
        public_contexts.push(PublicDecryptionContextSerde::<E> {
            domain: domain.to_vec(),
            public_key_shares: PublicKeyShares::<E> {
                public_key_shares: public.to_vec(),
            },
            blinded_key_shares: blinded_key_shares,
            lagrange_n_0,
        });
    }
    for private in private_contexts.iter_mut() {
        private.public_decryption_contexts = public_contexts.clone();
    }

    Setup {
        pubkey: pubkey.into(),
        contexts: private_contexts,
    }
}

pub fn generate_random<R: RngCore, E: ark_ec::pairing::Pairing>(
    n: usize,
    rng: &mut R,
) -> Vec<E::ScalarField> {
    (0..n)
        .map(|_| E::ScalarField::rand(rng))
        .collect::<Vec<_>>()
}

#[cfg(test)]
mod tests {
    use std::ops::{Div, Mul};

    use ark_bls12_381::{g2, Bls12_381, Fr};
    use ark_ec::{bls12::Bls12, short_weierstrass_jacobian::GroupProjective};
    use ark_ff::Fp12;
    use ark_poly::{DenseUVPolynomial, Polynomial};
    use ark_std::test_rng;

    use crate::*;

    type E = ark_bls12_381::Bls12_381;

    #[test]
    fn symmetric_encryption() {
        let mut rng = test_rng();
        let threshold = 3;
        let shares_num = 5;
        let num_entities = 5;

        let msg: &[u8] = "abc".as_bytes();

        let (pubkey, privkey, _, _domain_points) =
            setup::<E>(threshold, shares_num, num_entities);

        let ciphertext =
            encrypt::<ark_std::rand::rngs::StdRng, E>(msg, pubkey, &mut rng);
        let plaintext = decrypt(&ciphertext, privkey);

        assert!(msg == plaintext)
    }

    fn lagrange_poly(
        j: usize,
        indices: &Vec<usize>,
        domain_points: &Vec<Fr>,
    ) -> DensePolynomial<Fr> {
        let mut p = DensePolynomial::from_coefficients_vec(vec![Fr::one()]);
        for i in indices {
            if *i == j {
                continue;
            }
            let d = DensePolynomial::from_coefficients_vec(vec![
                domain_points[j].clone() - domain_points[*i].clone(),
            ]);
            let n = DensePolynomial::from_coefficients_vec(vec![
                -domain_points[*i].clone(),
                Fr::one(),
            ]);
            p = p.mul(&n.div(&d));
        }
        p
    }

    #[test]
    fn threshold_encryption() {
        let rng = &mut test_rng();
        //let threshold = 16 * 2 / 3;
        //let shares_num = 16;
        //let num_entities = 5;

        let threshold = 5 * 2 / 3;
        let shares_num = 5;
        let num_entities = 5;

        //let threshold = 16 * 2 / 3;
        println!("threshold = {}", threshold);
        //let shares_num = 16;
        //let num_entities = 16;

        let msg: &[u8] = "abc".as_bytes();
        println!("\n\n");
        let (pubkey, _privkey, contexts, domain_points) =
            setup::<E>(threshold, shares_num, num_entities);
        println!("\n\n");

        let indices: Vec<usize> = (0..contexts.len()).collect();
        let sk: GroupProjective<g2::Parameters> = contexts.iter().enumerate().map(|(i, c)| {
            let l = lagrange_poly(i, &indices, &domain_points);
            let eval = DensePolynomial::evaluate(&l, &Fr::zero());
            c.private_key_share.private_key_shares[0].mul(eval)
        }).fold(<Bls12<ark_bls12_381::Parameters> as ark_ec::pairing::Pairing>::G2Projective::zero(), |acc, x| acc + x);
        // check e(pubkey, h) == e(g,sk)
        let lhs: Fp12<ark_bls12_381::Fq12Parameters> = Bls12_381::pairing(
            pubkey,
            <Bls12<ark_bls12_381::Parameters> as ark_ec::pairing::Pairing>::G2Affine::prime_subgroup_generator());
        let rhs: Fp12<ark_bls12_381::Fq12Parameters> = Bls12_381::pairing(<Bls12<ark_bls12_381::Parameters> as ark_ec::pairing::Pairing>::G1Affine::prime_subgroup_generator(), sk);
        assert_eq!(lhs, rhs);

        let ciphertext = encrypt::<_, E>(msg, pubkey, rng);

        let mut shares: Vec<DecryptionShare<E>> = vec![];
        // fails with .take(threshold-1)
        for context in contexts.iter().take(threshold) {
            shares.push(context.create_share(&ciphertext));
        }

        println!("len shares= {}", shares.len());
        // TODO fix function verify_blinding
        /*for pub_context in contexts[0].public_decryption_contexts.iter() {
            assert!(pub_context
                .blinded_key_shares
                .verify_blinding(&pub_context.public_key_shares, rng));
        }*/
        let prepared_blinded_key_shares = contexts[0].prepare_combine(&shares);
        let s =
            contexts[0].share_combine(&shares, &prepared_blinded_key_shares);

        let plaintext = decrypt_with_shared_secret(&ciphertext, &s);
        println!("plaintext={:?}", plaintext);
        println!("msg={:?}", msg);
        assert!(plaintext == msg)
    }
}
