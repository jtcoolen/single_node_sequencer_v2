#![allow(non_snake_case)]
#![allow(dead_code)]

use serde::de::Error;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use crate::*;

#[derive(
    Debug, Clone, Deserialize, CanonicalSerialize, CanonicalDeserialize,
)]
pub struct DecryptionShare<E: ark_ec::pairing::Pairing> {
    pub decryptor_index: usize,
    #[serde(deserialize_with = "g1_from_bytes::<__D, E>")]
    pub decryption_share: E::G1Affine,
}

fn g1_from_bytes<'de, D, E: ark_ec::pairing::Pairing>(
    deserializer: D,
) -> Result<E::G1Affine, D::Error>
where
    D: Deserializer<'de>,
{
    let v: Vec<u8> = Deserialize::deserialize(deserializer)?;
    E::G1Affine::deserialize_compressed(&*v).map_err(D::Error::custom)
}

impl Serialize for DecryptionShare<Bls12_381> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("DecryptionShare", 3)?;

        state.serialize_field("decryptor_index", &self.decryptor_index)?;

        let mut decryption_share_bytes = Vec::new();
        self.decryption_share
            .serialize_compressed(&mut decryption_share_bytes)
            .unwrap();
        state.serialize_field("decryption_share", &decryption_share_bytes)?;
        state.end()
    }
}

impl<E: ark_ec::pairing::Pairing> PrivateDecryptionContext<E> {
    pub fn create_share(
        &self,
        ciphertext: &Ciphertext<E>,
    ) -> DecryptionShare<E> {
        let decryption_share = ciphertext.nonce.mul(self.b_inv).into_affine();

        DecryptionShare {
            decryptor_index: self.index,
            decryption_share,
        }
    }
    pub fn batch_verify_decryption_shares<R: RngCore>(
        &self,
        ciphertexts: &[Ciphertext<E>],
        shares: &[Vec<DecryptionShare<E>>],
        //ciphertexts_and_shares: &[(Ciphertext<E>, Vec<DecryptionShare<E>>)],
        rng: &mut R,
    ) -> bool {
        let num_ciphertexts = ciphertexts.len();
        let num_shares = shares[0].len();

        // Get [b_i] H for each of the decryption shares
        let blinding_keys = shares[0]
            .iter()
            .map(|d| {
                self.public_decryption_contexts[d.decryptor_index]
                    .blinded_key_shares
                    .blinding_key_prepared
                    .clone()
            })
            .collect::<Vec<_>>();

        // For each ciphertext, generate num_shares random scalars
        let alpha_ij = (0..num_ciphertexts)
            .map(|_| generate_random::<_, E>(num_shares, rng))
            .collect::<Vec<_>>();

        let mut pairings = Vec::with_capacity(num_shares + 1);

        // Compute \sum_i \alpha_{i,j} for each ciphertext j
        let sum_alpha_i = alpha_ij
            .iter()
            .map(|alpha_j| alpha_j.iter().sum::<E::ScalarField>())
            .collect::<Vec<_>>();

        // Compute \sum_j [ \sum_i \alpha_{i,j} ] U_j
        let sum_u_j = E::G1Prepared::from(
            izip!(ciphertexts.iter(), sum_alpha_i.iter())
                .map(|(c, alpha_j)| c.nonce.mul(*alpha_j))
                .sum::<E::G1>()
                .into_affine(),
        );

        // e(\sum_j [ \sum_i \alpha_{i,j} ] U_j, -H)
        pairings.push((sum_u_j, self.h_inv.clone()));

        let mut sum_d_j = vec![E::G1::zero(); num_shares];

        // sum_D_j = { [\sum_j \alpha_{i,j} ] D_i }
        for (d, alpha_j) in izip!(shares.iter(), alpha_ij.iter()) {
            for (sum_alpha_d_i, d_ij, alpha) in
                izip!(sum_d_j.iter_mut(), d.iter(), alpha_j.iter())
            {
                *sum_alpha_d_i += d_ij.decryption_share.mul(*alpha);
            }
        }

        // e([\sum_j \alpha_{i,j} ] D_i, B_i)
        /*for (d_i, b_i) in izip!(sum_d_j.iter(), blinding_keys.iter()) {
            pairings
                .push((E::G1Prepared::from(d_i.into_affine()), b_i.clone()));
        }*/

        E::multi_pairing(sum_d_j, blinding_keys).0 == E::TargetField::one()
    }
}

impl<E: ark_ec::pairing::Pairing> PrivateDecryptionContextSerde<E> {
    pub fn create_share(
        &self,
        ciphertext: &Ciphertext<E>,
    ) -> DecryptionShare<E> {
        let decryption_share = ciphertext.nonce.mul(self.b_inv).into_affine();

        DecryptionShare {
            decryptor_index: self.index,
            decryption_share,
        }
    }
    pub fn batch_verify_decryption_shares<R: RngCore>(
        &self,
        ciphertexts: &[Ciphertext<E>],
        shares: &[Vec<DecryptionShare<E>>],
        //ciphertexts_and_shares: &[(Ciphertext<E>, Vec<DecryptionShare<E>>)],
        rng: &mut R,
    ) -> bool {
        let num_ciphertexts = ciphertexts.len();
        let num_shares = shares[0].len();

        // Get [b_i] H for each of the decryption shares
        let blinding_keys = shares[0]
            .iter()
            .map(|d| {
                E::G2Prepared::from(
                    self.public_decryption_contexts[d.decryptor_index]
                        .blinded_key_shares
                        .blinding_key_prepared,
                )
            })
            .collect::<Vec<_>>();

        // For each ciphertext, generate num_shares random scalars
        let alpha_ij = (0..num_ciphertexts)
            .map(|_| generate_random::<_, E>(num_shares, rng))
            .collect::<Vec<_>>();

        let mut pairings = Vec::with_capacity(num_shares + 1);

        // Compute \sum_i \alpha_{i,j} for each ciphertext j
        let sum_alpha_i = alpha_ij
            .iter()
            .map(|alpha_j| alpha_j.iter().sum::<E::ScalarField>())
            .collect::<Vec<_>>();

        // Compute \sum_j [ \sum_i \alpha_{i,j} ] U_j
        let sum_u_j = E::G1Prepared::from(
            izip!(ciphertexts.iter(), sum_alpha_i.iter())
                .map(|(c, alpha_j)| c.nonce.mul(*alpha_j))
                .sum::<E::G1>()
                .into_affine(),
        );

        // e(\sum_j [ \sum_i \alpha_{i,j} ] U_j, -H)
        pairings.push((sum_u_j, E::G2Prepared::from(self.h_inv).clone()));

        let mut sum_d_j = vec![E::G1::zero(); num_shares];

        // sum_D_j = { [\sum_j \alpha_{i,j} ] D_i }
        for (d, alpha_j) in izip!(shares.iter(), alpha_ij.iter()) {
            for (sum_alpha_d_i, d_ij, alpha) in
                izip!(sum_d_j.iter_mut(), d.iter(), alpha_j.iter())
            {
                *sum_alpha_d_i += d_ij.decryption_share.mul(*alpha);
            }
        }

        // e([\sum_j \alpha_{i,j} ] D_i, B_i)
        /*for (d_i, b_i) in izip!(sum_d_j.iter(), blinding_keys.iter()) {
            pairings
                .push((E::G1Prepared::from(d_i.into_affine()), b_i.clone()));
        }*/

        E::multi_pairing(sum_d_j, blinding_keys).0 == E::TargetField::one()
    }
}
