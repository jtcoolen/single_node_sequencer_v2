use ark_bls12_381::Bls12_381;
use ark_ec::AffineRepr;
use ark_ff::{One, UniformRand};
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize};
use chacha20::cipher::{NewCipher, StreamCipher};
use chacha20::{ChaCha20, Key, Nonce};
use rand_core::RngCore;
use serde::de::Error;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::ops::{Mul, Neg};
use std::time::Instant;

use crate::{construct_tag_hash, hash_to_g2};

#[derive(
    Clone,
    Debug,
    PartialEq,
    Deserialize,
    CanonicalSerialize,
    CanonicalDeserialize,
)]
pub struct Ciphertext<E: ark_ec::pairing::Pairing> {
    #[serde(deserialize_with = "g1_from_bytes::<__D, E>")]
    pub nonce: E::G1Affine,
    // U
    pub ciphertext: Vec<u8>,
    // V
    #[serde(deserialize_with = "g2_from_bytes::<__D, E>")]
    pub auth_tag: E::G2Affine, // W
}

fn g1_from_bytes<'de, D, E: ark_ec::pairing::Pairing>(
    deserializer: D,
) -> Result<E::G1Affine, D::Error>
where
    D: Deserializer<'de>,
{
    let v: Vec<u8> = Deserialize::deserialize(deserializer)?;
    E::G1Affine::deserialize_compressed(&*v).map_err(D::Error::custom)
}

fn g2_from_bytes<'de, D, E: ark_ec::pairing::Pairing>(
    deserializer: D,
) -> Result<E::G2Affine, D::Error>
where
    D: Deserializer<'de>,
{
    let v: Vec<u8> = Deserialize::deserialize(deserializer)?;
    E::G2Affine::deserialize_compressed(&*v).map_err(D::Error::custom)
}

impl Serialize for Ciphertext<Bls12_381> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("Ciphertext", 3)?;

        let mut nonce_bytes: Vec<u8> = Vec::new();
        self.nonce.serialize_compressed(&mut nonce_bytes).unwrap();
        state.serialize_field("nonce", &nonce_bytes)?;

        state.serialize_field("ciphertext", &self.ciphertext)?;

        let mut auth_tag_bytes = Vec::new();
        self.auth_tag
            .serialize_compressed(&mut auth_tag_bytes)
            .unwrap();
        state.serialize_field("auth_tag", &auth_tag_bytes)?;
        state.end()
    }
}

impl<E: ark_ec::pairing::Pairing> Ciphertext<E> {
    pub fn check(&self, g_inv: &E::G1Prepared) -> bool {
        let hash_g2 = E::G2Prepared::from(self.construct_tag_hash());

        E::multi_pairing(
            [E::G1Prepared::from(self.nonce), g_inv.clone()],
            [hash_g2, E::G2Prepared::from(self.auth_tag)],
        )
        .0 == E::TargetField::one()
    }
    fn construct_tag_hash(&self) -> E::G2Affine {
        let mut hash_input = Vec::<u8>::new();
        self.nonce.serialize_compressed(&mut hash_input).unwrap();
        hash_input.extend_from_slice(&self.ciphertext);

        hash_to_g2(&hash_input)
    }
}

pub fn encrypt<R: RngCore, E: ark_ec::pairing::Pairing>(
    message: &[u8],
    pubkey: E::G1Affine,
    rng: &mut R,
) -> Ciphertext<E> {
    let now = Instant::now();
    // r
    let rand_element = E::ScalarField::rand(rng);
    // g
    let g_gen = E::G1Affine::generator();
    // h
    let h_gen = E::G2Affine::generator();

    let ry_prep = E::G1Prepared::from(pubkey.mul(rand_element).into());
    // s
    let product = E::pairing(ry_prep, h_gen).0;
    // u
    let blinded = g_gen.mul(rand_element).into();

    println!("ECC ops duration {:?}", now.elapsed());

    let now = Instant::now();
    let mut cipher = shared_secret_to_chacha::<E>(&product);

    println!("shared_secret_to_chacha duration {:?}", now.elapsed());
    let mut msg = message.to_vec();

    let now = Instant::now();
    cipher.apply_keystream(&mut msg);
    println!("apply_keystream duration {:?}", now.elapsed());

    let now = Instant::now();
    let tag = construct_tag_hash::<E>(blinded, &msg[..])
        .mul(rand_element)
        .into();
    println!("construct_tag_hash duration {:?}", now.elapsed());

    Ciphertext::<E> {
        nonce: blinded,
        ciphertext: msg,
        auth_tag: tag,
    }
}

pub fn check_ciphertext_validity<E: ark_ec::pairing::Pairing>(
    c: &Ciphertext<E>,
) -> bool {
    let g_inv =
        E::G1Prepared::from(E::G1Affine::generator().into_group().neg());
    let hash_g2 = E::G2Prepared::from(construct_tag_hash::<E>(
        c.nonce,
        &c.ciphertext[..],
    ));

    E::multi_pairing(
        [E::G1Prepared::from(c.nonce), g_inv],
        [hash_g2, E::G2Prepared::from(c.auth_tag)],
    )
    .0 == E::TargetField::one()
}

pub fn decrypt<E: ark_ec::pairing::Pairing>(
    ciphertext: &Ciphertext<E>,
    privkey: E::G2Affine,
) -> Vec<u8> {
    let s = E::pairing(
        E::G1Prepared::from(ciphertext.nonce),
        E::G2Prepared::from(privkey),
    )
    .0;
    decrypt_with_shared_secret(ciphertext, &s)
}

pub fn decrypt_with_shared_secret<E: ark_ec::pairing::Pairing>(
    ciphertext: &Ciphertext<E>,
    s: &E::TargetField,
) -> Vec<u8> {
    let mut plaintext = ciphertext.ciphertext.to_vec();
    let mut cipher = shared_secret_to_chacha::<E>(s);
    cipher.apply_keystream(&mut plaintext);

    plaintext
}

pub fn shared_secret_to_chacha<E: ark_ec::pairing::Pairing>(
    s: &E::TargetField,
) -> ChaCha20 {
    let mut prf_key = Vec::new();
    s.serialize_compressed(&mut prf_key).unwrap();
    let mut blake_params = blake2b_simd::Params::new();
    blake_params.hash_length(32);
    let mut hasher = blake_params.to_state();
    prf_key.serialize_compressed(&mut hasher).unwrap();
    let mut prf_key_32 = [0u8; 32];
    prf_key_32.clone_from_slice(hasher.finalize().as_bytes());
    let chacha_nonce = Nonce::from_slice(b"secret nonce");
    ChaCha20::new(Key::from_slice(&prf_key_32), chacha_nonce)
}
