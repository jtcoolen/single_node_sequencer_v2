use ark_ec::AffineRepr;

pub fn batch_to_projective<A: AffineRepr>(p: &[A]) -> Vec<A::Group> {
    p.iter().map(|a| a.into_group()).collect::<Vec<_>>()
}
