use std::ops::{Div, Mul};

pub use ark_bls12_381::Bls12_381 as EllipticCurve;
use ark_bls12_381::{Bls12_381, Fr};
use ark_ec::bls12::Bls12;
use ark_ec::short_weierstrass_jacobian::GroupProjective;
use ark_ec::{AffineCurve, PairingEngine, ProjectiveCurve};
use ark_ff::{Field, Fp12, One, Zero};
use ark_poly::univariate::DensePolynomial;
use ark_poly::{EvaluationDomain, Polynomial, UVPolynomial};
use ferveo::dkg::{DkgState, Message, PubliclyVerifiableDkg};
use ferveo::*;
use ferveo_common::{Keypair, TendermintValidator, ValidatorSet};
use measure_time::print_time;

pub fn main() {
    setup_dealt_dkg(5, 5);
    /*setup_dealt_dkg(10, 2048);
    setup_dealt_dkg(10, 4096);
    setup_dealt_dkg(10, 8192);*/
}

/// Generate a set of keypairs for each validator
pub fn gen_keypairs(num: u64) -> Vec<ferveo_common::Keypair<EllipticCurve>> {
    let rng = &mut ark_std::test_rng();
    (0..num)
        .map(|_| ferveo_common::Keypair::<EllipticCurve>::new(rng))
        .collect()
}

/// Generate a few validators
pub fn gen_validators(
    keypairs: &[ferveo_common::Keypair<EllipticCurve>],
) -> ValidatorSet<EllipticCurve> {
    ValidatorSet::new(
        (0..keypairs.len())
            .map(|i| TendermintValidator {
                power: 5 / keypairs.len() as u64, // todo remove hardcoded constant 5
                address: format!("validator_{}", i),
                public_key: keypairs[i as usize].public(),
            })
            .collect(),
    )
}

/// Create a test dkg in state [`DkgState::Init`]
pub fn setup_dkg(
    keypairs: &Vec<Keypair<Bls12_381>>,
    validators: &ValidatorSet<Bls12_381>,
    validator: usize,
    shares: u32,
) -> PubliclyVerifiableDkg<EllipticCurve> {
    let me = validators.validators[validator].clone();
    let dkg = PubliclyVerifiableDkg::new(
        validators.clone(),
        Params {
            tau: 0,
            security_threshold: shares * 2 / 3,
            total_weight: shares,
            retry_after: 1,
        },
        me,
        keypairs[validator].clone(),
    )
    .expect("Setup failed");
    dkg
}

fn lagrange_poly(
    j: usize,
    indices: &Vec<usize>,
    domain_points: &Vec<Fr>,
) -> DensePolynomial<Fr> {
    let mut p = DensePolynomial::from_coefficients_vec(vec![Fr::one()]);
    for i in indices {
        if *i == j {
            continue;
        }
        let d = DensePolynomial::from_coefficients_vec(vec![
            domain_points[j].clone() - domain_points[*i].clone(),
        ]);
        let n = DensePolynomial::from_coefficients_vec(vec![
            -domain_points[*i].clone(),
            Fr::one(),
        ]);
        p = p.mul(&n.div(&d));
    }
    p
}

/// Set up a dkg with enough pvss transcripts to meet the threshold
pub fn setup_dealt_dkg(num: u64, shares: u32) {
    let rng = &mut ark_std::test_rng();
    // gather everyone's transcripts
    let mut transcripts: Vec<Message<Bls12_381>> = vec![];
    let mut contexts: Vec<PubliclyVerifiableDkg<Bls12_381>> = vec![];

    // each validator generates a uniformly random session key
    let keypairs: Vec<Keypair<Bls12_381>> = gen_keypairs(num);
    let validators: ValidatorSet<Bls12_381> = gen_validators(&keypairs);
    let _params = Params {
        tau: 0,
        security_threshold: shares * 2 / 3,
        total_weight: shares,
        retry_after: 1,
    };

    //let validators_set = partition_domain(&params, validators.clone())
    //   .expect("partition_domain_dealt_dkg_failure");

    // each validator runs PartitionDomain
    for i in 0..num {
        let dkg: PubliclyVerifiableDkg<Bls12_381> =
            setup_dkg(&keypairs, &validators, i as usize, shares);
        contexts.push(dkg);
        // transcripts.push(contexts[i as usize].share(rng).expect("Test failed"));
    }

    // each validator runs Scrape.Deal
    for participant in contexts.iter_mut() {
        println!("hello participant");
        let msg = participant.share(rng).unwrap();
        let msg: Message<ark_bls12_381::Bls12_381> = msg; //.verify().unwrap().1;
        transcripts.push(msg); // do as in setup_dealt_dkg()
                               //messages.push_back(msg);
    }

    // our test dkg
    /*let dkg: &mut PubliclyVerifiableDkg<Bls12<ark_bls12_381::Parameters>> = &mut contexts[0];
    // iterate over transcripts from lowest weight to highest
    for (sender, pvss) in transcripts.into_iter().rev().enumerate() {
        if let Message::Deal(ss) = pvss.clone() {
            print_time!("PVSS verify pvdkg");
            ss.verify_full(&dkg, rng);
        }
        dkg.apply_message(
            dkg.validators[num as usize - 1 - sender].validator.clone(),
            pvss,
        )
            .expect("Setup failed");
    }*/

    // for each validator
    for (sender, pvss) in transcripts.into_iter().rev().enumerate() {
        // for each validator
        for node in contexts.iter_mut() {
            println!("hello node");
            //node.apply_message(msg.0, &msg.1.clone()).unwrap();
            if let Message::Deal(ss) = pvss.clone() {
                print_time!("PVSS verify pvdkg");
                // Verify individual PVSS message: DKG.VerifyPVSS with PVSS from DKG and new PVSS_i
                ss.verify_full(&node, rng);
            }
            // AggregatePVSS+VerifyAggregatedPVSS:
            node.apply_message(
                node.validators[num as usize - 1 - sender].validator.clone(),
                pvss.clone(),
            )
            .expect("Setup failed");
        }
    }

    let mut aggregated_pvss: Vec<
        dkg::Message<Bls12<ark_bls12_381::Parameters>>,
    > = vec![];
    for node in contexts.iter_mut() {
        aggregated_pvss.push(node.aggregate().expect("aggregation failed"));
    }

    for node in contexts.iter_mut() {
        println!("Node state is {:?}", node.state);
    }
    for (sender, pvss) in aggregated_pvss.iter().rev().enumerate() {
        for node in contexts.iter_mut() {
            /*node.verify_message(&validators.validators[sender], &pvss, rng)
            .expect("aggregation message check failed");*/
            if let Message::Aggregate(_) = pvss.clone() {
                //println!("cool aggregate {}", sender);
            }
            //println!("message {:?}", pvss.clone());
            match node.state {
                DkgState::Success { final_key: _ } => {}
                _ => {
                    node.apply_message(
                        node.validators[num as usize - 1 - sender]
                            .validator
                            .clone(),
                        pvss.clone(),
                    )
                    .expect("Setup failed");
                }
            }
        }
    }
    for node in contexts.iter() {
        println!("Node state is {:?}", node.state);
        match node.state {
            DkgState::Success { .. } => (),
            _ => assert!(false),
        }
    }

    let fft_domain =
        ark_poly::Radix2EvaluationDomain::<Fr>::new(shares as usize).unwrap();
    let mut domain_points: Vec<Fr> = Vec::with_capacity(shares as usize);
    let mut point = Fr::one();

    for _ in 0..shares {
        domain_points.push(point);
        point *= fft_domain.group_gen;
    }

    let pubkey = contexts[0].final_key();

    println!("pubkey {:?}", pubkey);

    for ctx in contexts.iter_mut() {
        let k = ctx.final_key();
        assert_eq!(k, pubkey);
    }

    let indices: Vec<usize> = (0..contexts.len()).collect();
    let sk: GroupProjective<ark_bls12_381::g2::Parameters> = contexts.iter().enumerate().map(|(i, c)| {
        let l = lagrange_poly(i, &indices, &domain_points);
        let eval: Fr = DensePolynomial::evaluate(&l, &Fr::zero());
        let dk_inv = keypairs[i].clone().decryption_key.inverse().unwrap();
        let private_key_shares : Vec<<Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Projective>  = c.local_shares.iter().map(|s| s.mul(dk_inv)).collect();
        /*let private_key_share = PrivateKeyShare::<ark_bls12_381::Bls12_381> {
            // See part validator decryption of private key shares https://nikkolasg.github.io/ferveo/pvss.html
            private_key_shares: private_key_shares.iter().map(|s| s.into_affine()).collect(), // ugly!!!!!! todo: we have to compute the private key shares Z(i,w_j)
        };*/
        private_key_shares[0].into_affine().mul(eval)
    }).fold(<Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Projective::zero(), |acc, x| acc + x);
    // check e(pubkey, h) == e(g,sk)
    let _lhs: Fp12<ark_bls12_381::Fq12Parameters> = Bls12_381::pairing(
        pubkey,
        <Bls12<ark_bls12_381::Parameters> as PairingEngine>::G2Affine::prime_subgroup_generator());
    let _rhs: Fp12<ark_bls12_381::Fq12Parameters> = Bls12_381::pairing(<Bls12<ark_bls12_381::Parameters> as PairingEngine>::G1Affine::prime_subgroup_generator(), sk);
    assert_eq!(_lhs, _rhs);
    println!("finished!")
}
