use std::error::Error as StdError;
use std::net::{IpAddr, SocketAddr};
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Arc;
use std::time::Duration;

use ark_bls12_381::Bls12_381;
use async_channel::{Receiver, Sender};
use clap::Parser;
use futures::{prelude::*, FutureExt};
use group_threshold_cryptography::{
    encrypt, Ciphertext, DecryptionShare, PrivateDecryptionContextSerde, Setup,
};
use hyper::server::conn::AddrStream;
use jsonrpsee::core::async_trait;
use jsonrpsee::proc_macros::rpc;
use jsonrpsee::server::http::response;
use jsonrpsee::server::ws;
use jsonrpsee::server::{
    stop_channel, ConnectionGuard, ConnectionState, RpcServiceBuilder, ServerConfig, StopHandle,
};
use jsonrpsee::types::ErrorObjectOwned;
use jsonrpsee::ws_client::WsClientBuilder;
use jsonrpsee::Methods;
use jsonrpsee_core::client::{Client, ClientT};
use jsonrpsee_core::params::ArrayParams;
use jsonrpsee_core::rpc_params;
use tokio::join;
use tokio::sync::{Mutex, MutexGuard};
use tracing::info;

#[derive(Parser, Debug)]
struct Args {
    /// Rpc Address that the sequencer will listen to:
    #[arg(long, default_value = "0.0.0.0")]
    host: IpAddr,

    /// Port that the sequencer will listen to
    #[arg(long, default_value = "9949")]
    port: u16,

    /// Upper limit of transactions that will be added in a preblock
    #[arg(short, long, default_value = "200")]
    transactions_per_preblock: usize,
    /// Time between preblocks, in milliseconds
    #[arg(short, long, default_value = "500")]
    preblock_time: u64,
    /// Multiplying factor for preblock time in case of empty preblocks
    #[arg(short, long, default_value = "12")]
    empty_preblocks_time_multiplying_factor: u16,
    ///Ips of the sequencer and key-holders. The first ip is the ip of the sequencer and is ignored
    #[arg(index = 1, value_delimiter = ',')]
    ips: Vec<IpAddr>,
}

async fn mk_client(url: String) -> Option<Arc<Client>> {
    match WsClientBuilder::default().build(url).await {
        Ok(c) => Some(Arc::new(c)),
        Err(_) => None,
    }
}

type Transaction = Ciphertext<Bls12_381>;
type Transactions = Vec<Transaction>;

type PreBlock = (usize, Transactions);

#[derive(Clone)]
struct PreBlocks {
    elements: Arc<Mutex<Vec<PreBlock>>>,
    sender: Sender<PreBlock>,
    receiver: Receiver<PreBlock>,
}

impl PreBlocks {
    fn new() -> Self {
        let (sender, receiver) = async_channel::unbounded();
        PreBlocks {
            elements: Arc::new(Mutex::new(vec![])),
            sender,
            receiver,
        }
    }

    async fn push(&mut self, level: usize, txs: Vec<Transaction>) {
        let pb = (level, txs.clone());
        self.elements.lock().await.push(pb.clone());
        let sender = self.sender.clone();
        // Send the item to the sender stream
        sender.send(pb).await.unwrap();
        info!("pushed preblock");
    }
}

type TransactionsPool = Arc<Mutex<Vec<Ciphertext<Bls12_381>>>>;

#[rpc(server)]
pub trait Api {
    #[method(name = "sequence_transaction")]
    async fn sequence_transaction(&self, tx: Transaction) -> Result<(), ErrorObjectOwned>;
}

struct RpcImpl {
    transactions_pool: TransactionsPool,
    n_txs: Arc<Mutex<u64>>,
}

async fn new(transactions_pool: TransactionsPool) -> RpcImpl {
    RpcImpl {
        transactions_pool,
        n_txs: Arc::new(Mutex::new(0)),
    }
}

#[async_trait]
impl ApiServer for RpcImpl {
    async fn sequence_transaction(
        &self,
        tx: Ciphertext<Bls12_381>,
    ) -> Result<(), ErrorObjectOwned> {
        let mut binding = self.n_txs.lock().await;
        {
            *binding += 1;
            self.transactions_pool.lock().await.push(tx);
        }

        info!("received {} txs so far", binding);
        Ok(())
    }
}

async fn push_preblocks(
    preblocks: &mut PreBlocks,
    time_between_empty_blocks: u16,
    time_between_non_empty_blocks: Duration,
    transactions_pool: Arc<Mutex<Vec<Ciphertext<Bls12_381>>>>,
    max_txs_in_block: usize,
    _key_holders: Arc<Mutex<Vec<Option<Arc<Client>>>>>,
) {
    let mut block_level: usize = 0;
    let mut count = 1;
    let mut n_txs = 0;
    loop {
        tokio::time::sleep(time_between_non_empty_blocks).await;
        let mut tx_pool = transactions_pool.lock().await;
        if count == time_between_empty_blocks && tx_pool.is_empty() {
            preblocks.push(block_level, Vec::new()).await;
            let len = tx_pool.len();
            info!("Created empty preblock {} at level {}", len, block_level);
            count = 0;
            block_level += 1;
        } else if tx_pool.is_empty() {
            count += 1;
        } else {
            let len = tx_pool.len();
            let final_length = if len < max_txs_in_block {
                len
            } else {
                max_txs_in_block
            };
            n_txs += final_length;
            preblocks
                .push(block_level, tx_pool.drain(0..final_length).collect())
                .await;
            info!(
                "Created preblock with {} transactions at level {}",
                final_length, block_level
            );
            block_level += 1;
            info!("sequenced {} tx so far", n_txs);
        }
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    let time_between_non_empty_blocks = Duration::from_millis(args.preblock_time);

    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();

    let ips = args.ips;

    let port = args.port;
    let host = args.host;

    let transactions_pool = Arc::new(Mutex::new(Vec::new()));

    let mut preblocks = PreBlocks::new();
    let receiver = preblocks.receiver.clone();

    //let client = Arc::new(WsClientBuilder::default().build("ws://127.0.0.1:9940").await.unwrap());
    let n_clients = 6;
    let key_holders =
        (0..n_clients).map(|i| mk_client(format!("ws://{}:64001", ips.get(i + 1).unwrap())));
    let key_holders: Vec<Option<Arc<Client>>> = futures::future::join_all(key_holders).await;
    let key_holders = Arc::new(Mutex::new(key_holders));

    let a = run_server(
        &host,
        port,
        transactions_pool.clone(),
        receiver,
        key_holders.clone(),
        ips,
    );
    let b = tokio::task::spawn(async move {
        push_preblocks(
            &mut preblocks,
            args.empty_preblocks_time_multiplying_factor,
            time_between_non_empty_blocks,
            transactions_pool,
            args.transactions_per_preblock,
            key_holders,
        )
        .await
    });
    info!("Server listening at: {}:{}", host, port);

    let _ = join!(a, b);

    Ok(())
}

async fn run_server(
    host: &IpAddr,
    port: u16,
    tx_pool: TransactionsPool,
    receiver: Receiver<PreBlock>,
    key_holders: Arc<Mutex<Vec<Option<Arc<Client>>>>>,
    ips: Vec<IpAddr>,
) {
    use hyper::service::{make_service_fn, service_fn};

    let addr = SocketAddr::from((*host, port));

    // This state is cloned for every connection
    // all these types based on Arcs and it should
    // be relatively cheap to clone them.
    //
    // Make sure that nothing expensive is cloned here
    // when doing this or use an `Arc`.
    #[derive(Clone)]
    struct PerConnection {
        methods: Methods,
        stop_handle: StopHandle,
        conn_id: Arc<AtomicU32>,
        conn_guard: ConnectionGuard,
    }

    // Each RPC call/connection get its own `stop_handle`
    // to able to determine whether the server has been stopped or not.
    //
    // To keep the server running the `server_handle`
    // must be kept and it can also be used to stop the server.
    let (stop_handle, _server_handle) = stop_channel();

    let rpc = new(tx_pool).await;

    // TODO: per_conn should be an arc
    let per_conn = PerConnection {
        methods: rpc.into_rpc().into(),
        stop_handle: stop_handle.clone(),
        conn_id: Default::default(),
        conn_guard: ConnectionGuard::new(100),
    };

    let ips = ips.clone();

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(move |_conn: &AddrStream| {
        let per_conn = per_conn.clone();
        let receiver = receiver.clone();
        let remote_addr = _conn.remote_addr();
        info!("remote addr: {}", remote_addr);
        let key_holders = key_holders.clone();

        let ips = ips.clone();
        async move {
            Ok::<_, Box<dyn StdError + Send + Sync>>(service_fn(move |req| {
                let is_websocket = ws::is_upgrade_request(&req);
                let PerConnection {
                    methods,
                    stop_handle,
                    conn_id,
                    conn_guard,
                } = per_conn.clone();

                // jsonrpsee expects a `conn permit` for each connection.
                //
                // This may be omitted if don't want to limit the number of connections
                // to the server.
                let Some(conn_permit) = conn_guard.try_acquire() else {
                    return async {
                        Ok::<_, Box<dyn StdError + Send + Sync>>(response::too_many_requests())
                    }
                    .boxed();
                };

                // You can't determine whether the websocket upgrade handshake failed or not here.

                if is_websocket {
                    let rpc_service = RpcServiceBuilder::new();

                    let conn = ConnectionState::new(
                        stop_handle.clone(),
                        conn_id.fetch_add(1, Ordering::Relaxed),
                        conn_permit,
                    );
                    let receiver = receiver.clone();
                    let key_holders = key_holders.clone();

                    let ips = ips.clone();
                    async move {

                        // Establishes the websocket connection
                        // the connection is closed i.e, when the `conn_fut` is dropped.
                        match ws::connect(req, ServerConfig::default(), methods.clone(), conn, rpc_service).await {
                            Ok((rp, conn_fut)) => {
                                tokio::task::spawn(async move {
                                    // Keep the connection alive until
                                    // a close signal is sent.
                                    conn_fut.await;
                                });

                                tokio::task::spawn(async move {
                                    tokio::time::sleep(Duration::new(1, 0)).await; // hack, waiting for the remote RPC server to be up

                                    while let Ok(preblock) = receiver.recv().await {
                                        let mut khs: MutexGuard<Vec<Option<Arc<Client>>>> = key_holders.lock().await;
                                        let new_khs: Arc<Mutex<Vec<Option<Arc<Client>>>>> = Arc::new(Mutex::new(vec![None; 6]));
                                        let _ = futures::stream::iter::<&Vec<Option<Arc<Client>>>>(&khs).enumerate().for_each(|(i, kh)| {
                                            let preblock = preblock.clone();
                                            let new_khs = new_khs.clone();
                                            let ips = ips.clone();
                                            async move {
                                                let ips: Vec<IpAddr> = ips.clone();
                                                let addr: &IpAddr = ips.get(i + 1).unwrap();
                                                let addr = format!("ws://{}:64001", addr);
                                                match kh {
                                                    None => {
                                                        let kh = mk_client(addr.clone()).await;
                                                        match kh {
                                                            Some(kh) => {
                                                                kh.request::<(), ArrayParams>("submit_preblock", rpc_params![preblock]).await.unwrap();
                                                                let mut new_khs = new_khs.lock().await;
                                                                new_khs[i] = Some(kh.clone());
                                                            }
                                                            None => {
                                                                info!("could not connect to {}", addr);
                                                            }
                                                        }
                                                    }
                                                    Some(kh) =>
                                                        {
                                                            // todo move to pool task to avoid waiting for response here
                                                            let kh = kh.clone();
                                                            let _ = tokio::task::spawn(async move {
                                                                _ = kh.request::<(), ArrayParams>("submit_preblock", rpc_params![preblock]).await
                                                            });
                                                        }
                                                };
                                            }
                                        }).await;

                                        let new_khs = new_khs.lock().await;
                                        for (i, nkh) in new_khs.iter().enumerate() {
                                            if let Some(nkh) = nkh {
                                                khs[i] = Some(nkh.clone());
                                            }
                                        }

                                        info!("New preblock added to queue: {:?} & broadcasted preblocks to key holders", preblock);
                                    }
                                });
                                Ok(rp)
                            }
                            Err(rp) => {
                                Ok(rp)
                            }
                        }
                    }
                        .boxed()
                } else if !ws::is_upgrade_request(&req) {
                    let rpc_service = RpcServiceBuilder::new();

                    let server_cfg = ServerConfig::default();
                    let conn = ConnectionState::new(
                        stop_handle,
                        conn_id.fetch_add(1, Ordering::Relaxed),
                        conn_permit,
                    );

                    // There is another API for making call with just a service as well.
                    //
                    // See [`jsonrpsee::server::http::call_with_service`]
                    async move {
                        // Rpc call finished successfully.
                        Ok(jsonrpsee::server::http::call_with_service_builder(
                            req,
                            server_cfg,
                            conn,
                            methods,
                            rpc_service,
                        )
                        .await)
                    }
                    .boxed()
                } else {
                    async { Ok(http::response::Response::default()) }.boxed()
                }
            }))
        }
    });

    let server = hyper::Server::bind(&addr).serve(make_service);

    let _ = server.await;
}
