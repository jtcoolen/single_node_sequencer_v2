fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::io::BufWriter;

    use ark_bls12_381::{Bls12_381, Fq12Config, Fr};
    use ark_ec::bls12::{G1Affine, G2Affine, G2Prepared};
    use ark_ff::Fp12;
    use ark_serialize::{CanonicalDeserialize, CanonicalSerialize};
    use ark_std::rand::SeedableRng;

    use ferveo_common::TendermintValidator;
    use ferveo_common::ValidatorSet;

    // TODO:  initial publicly verifiable DKG phase
    // each key holder:
    // - computes its keypair, validator info
    // - broadcasts its validator info => needs RPC and event loop
    // - runs setup_dkg upon reception of all validators' info
    // - broadcasts the output of dkg.share(rng) to all validators ==> needs transcript RPC & event loop
    // - upon reception of all transcripts, does ss.verify_full(&dkg, rng) & apply_message as in setup_dealt_dkg

    /// Generate a set of keypairs for each validator
    /// DKG.GenerateEpochKeyPair
    pub fn gen_keypairs<EllipticCurve: ark_ec::pairing::Pairing>(
        num: u64,
    ) -> Vec<ferveo_common::Keypair<EllipticCurve>> {
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0);
        (0..num)
            .map(|_| ferveo_common::Keypair::<EllipticCurve>::new(rng))
            .collect()
    }

    /// Generate a few validators
    pub fn gen_validators<EllipticCurve: ark_ec::pairing::Pairing>(
        keypairs: &[ferveo_common::Keypair<EllipticCurve>],
    ) -> ValidatorSet<EllipticCurve> {
        let v = (0..keypairs.len())
            .map(|i| TendermintValidator {
                power: 5 / keypairs.len() as u64, // TODO remove hardcoded constant 5
                address: format!("validator_{}", i),
                public_key: keypairs[i].public(),
            })
            .collect::<Vec<_>>();
        ValidatorSet::new(v)
    }

    // taken and adapted from https://github.com/anoma/ferveo/commit/8ecdb48c83ead0ddbb57a295c7732893b5a566fb
    #[test]
    pub fn test_pvdkg_tpke() {
        use ark_bls12_381::Config;
        use group_threshold_cryptography::*;
        //let rng = &mut ark_std::test_rng();
        //let ed_rng = &mut rand::rngs::StdRng::from_seed([0u8; 32]);

        let threshold = 6 * 2 / 3;
        let shares_num = 6;
        let num_entities = 6;

        let msg: &[u8] = "abcdefghijklmnopqrstuvwxyz".as_bytes();

        let rng = &mut ark_std::test_rng();

        assert!(shares_num >= threshold as usize);

        let (pubkey, _privkey, contexts, _): (
            G1Affine<Config>,
            G2Affine<Config>,
            Vec<PrivateDecryptionContext<Bls12_381>>,
            Vec<Fr>,
        ) = setup::<ark_bls12_381::Bls12_381>(threshold as usize, shares_num, num_entities);

        let setup =
            setup2::<ark_bls12_381::Bls12_381>(threshold as usize, shares_num, num_entities);

        let mut buf = BufWriter::new(Vec::new());

        let () = setup.serialize_compressed(&mut buf).unwrap();

        let bytes = buf.into_inner().unwrap();
        let setup2: Setup<Bls12_381> = Setup::deserialize_compressed(bytes.as_slice()).unwrap();
        assert_eq!(setup, setup2);

        fs::write("setup", bytes).expect("Unable to write file");

        let setup3 = fs::read("setup").expect("Unable to read file");
        let setup3: Setup<Bls12_381> = Setup::deserialize_compressed(setup3.as_slice()).unwrap();

        assert_eq!(setup, setup3);

        // See https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=f613e2a76843153d19adcd7c59f2766334f799bf
        // section 3.2.2 function E(pk)(m)
        // TE + Symmetric encryption
        let ciphertext: Ciphertext<Bls12_381> =
            encrypt::<_, ark_bls12_381::Bls12_381>(msg, pubkey, rng);

        // create Decryption Shares
        // Generate key shares
        let mut shares: Vec<DecryptionShare<ark_bls12_381::Bls12_381>> = vec![];
        println!("thresh {}", threshold);
        // taking less than threshold results in an invalid decryption
        for context in contexts.iter().take(threshold) {
            shares.push(context.create_share(&ciphertext));
        }
        println!("len shares= {}", shares.len());

        // TE Combine + Symmetric decryption
        let prepared_blinded_key_shares: Vec<G2Prepared<Config>> =
            contexts[0].prepare_combine(&shares);
        let s: Fp12<Fq12Config> =
            contexts[0].share_combine(&shares, &prepared_blinded_key_shares);

        let plaintext = decrypt_with_shared_secret(&ciphertext, &s);
        println!("plaintext={:?}", plaintext);
        println!("plaintext={:?}", std::str::from_utf8(&plaintext));
        println!("msg={:?}", msg);
        assert_eq!(plaintext, msg)
    }
}
